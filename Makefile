HOME_TREE := .
TCP_UDP_DIR := TCP_UDP
BOT_DIR := Bot
ADMIN_DIR := Admin
CANDC_DIR := CandC
IPC_DIR := IPC
FLUX_DIR := Flux
SERVEURWEB_DIR := ServeurWeb

all: MAKE_BOT MAKE_CANDC MAKE_ADMIN MAKE_SERVEURWEB

lib: MAKE_TCP_UDP MAKE_IPC MAKE_FLUX

MAKE_TCP_UDP:
	@(cd $(TCP_UDP_DIR) && $(MAKE))

MAKE_BOT: MAKE_TCP_UDP MAKE_FLUX
	@(cd $(BOT_DIR) && $(MAKE))

MAKE_ADMIN: MAKE_IPC
	@(cd $(ADMIN_DIR) && $(MAKE))

MAKE_CANDC: MAKE_TCP_UDP MAKE_IPC MAKE_FLUX
	@(cd $(CANDC_DIR) && $(MAKE))

MAKE_IPC:
	@(cd $(IPC_DIR) && $(MAKE))

MAKE_FLUX:
	@(cd $(FLUX_DIR) && $(MAKE))

MAKE_SERVEURWEB: MAKE_IPC MAKE_TCP_UDP MAKE_FLUX
	@(cd $(SERVEURWEB_DIR) && $(MAKE))

clean:
	@(cd $(TCP_UDP_DIR) && make clean_lib)
	@(cd $(IPC_DIR) && make clean)
	@(cd $(FLUX_DIR) && make clean)
	@(cd $(ADMIN_DIR) && make clean)
	@(cd $(BOT_DIR) && make clean)
	@(cd $(CANDC_DIR) && make clean)
	@(cd $(SERVEURWEB_DIR) && make clean)

man:
	more README.md
