#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <pthread.h>
#include <poll.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>

#include "../IPC/libIPC.h"
#include "../TCP_UDP/libTCP.h"
#include "../TCP_UDP/libUDP.h"
#include "../Flux/libFlux.h"

#define fileAttente 10
#define IP "192.168.0.33"
#define MAX_TAMPON 1024

typedef struct node node;

struct node{
  char id[MAX_TAMPON];
  char hote[MAX_TAMPON];
  FILE * fd;
  int tmp_vie;
  int last_msg;
  pthread_mutex_t SEM_socket;
  node * nextNode;
};
 
typedef struct{
  int socket;
} argumentUDP;

typedef struct{
  char ip[MAX_TAMPON];
  char charge[MAX_TAMPON];
  unsigned char * file;
  int taille_file;
  char * nom_file;
  int taille_nom;
  int FDMreponse;
  int FDMtype;
  int idExecution;
  int isWeb;
  char * path;
  node * bot;
} argumentsAction;


void * handleUDP;
int socketUDP, socketTCP;
node * listeBot;
unsigned char * file;
int taille_file = 0;
char * nom_file;
int taille_nom = 0;
int new_file = 0;
pthread_mutex_t semListeBot;
pthread_mutex_t semFile;


int fonction(unsigned char * msg, char * hote, int taille){
  if(taille == 10){
    SEM_lock(&semListeBot);
    unsigned char * pt_msg = msg;
    char * id;
    id = malloc(sizeof(char) * 6);
    int tmp;
    memcpy(id,msg,6 * sizeof(unsigned char));
    pt_msg = msg + 6 * sizeof(unsigned char);
    memcpy(&tmp, pt_msg, 4* sizeof(unsigned char));
    
    if(listeBot == NULL){
      listeBot = malloc(sizeof(node));
      strcpy(listeBot->id, (char *)id);
      strcpy(listeBot->hote,(char *) hote);
      listeBot->tmp_vie = tmp;
      listeBot->last_msg = time(NULL);
      listeBot->fd = NULL;
      listeBot->nextNode = NULL;
    }
    else{
      node * bot = listeBot;
      while(bot->nextNode != NULL){
	if(strcmp(bot->id, id) == 0){
	  break;
	}
	bot = bot->nextNode;
      }
      if(strcmp(bot->id,(char *)id) != 0){
	bot->nextNode  = malloc(sizeof(node));
	strcpy(bot->nextNode->id, (char *)id);
	strcpy(bot->nextNode->hote, (char *)hote);
	bot->nextNode->tmp_vie = tmp;
        bot->nextNode->last_msg = time(NULL);
	bot->nextNode->fd = NULL;
	bot->nextNode->nextNode = NULL;
      }
      else{
	if(tmp >= bot->tmp_vie){
	  bot->tmp_vie = tmp;
	  bot->last_msg = time(NULL);
	}
	else{
	  bot->tmp_vie = tmp;
	  bot->last_msg = time(NULL);
	  
	  SEM_lock(&(bot->SEM_socket));
	  if(bot->fd != NULL){
	    fclose(bot->fd);
	    bot->fd = NULL;
	  }
	  SEM_unlock(&(bot->SEM_socket));
	}
      }
    }
    free(id);
    
    SEM_unlock(&semListeBot);
  }
  return 0;
}

void cleanBotAFK(void * arg){
  while(1){
    SEM_lock(&semListeBot);
    if(listeBot != NULL){
      node * bot;
      int tmp_actuel = time(NULL);
      if((tmp_actuel - listeBot->last_msg) > 10){
	bot = listeBot;
	listeBot = listeBot->nextNode;

	SEM_lock(&(bot->SEM_socket));
	if(bot->fd != NULL){
	  fclose(bot->fd);
	  bot->fd = NULL;
	}
	SEM_unlock(&(bot->SEM_socket));
	
	free(bot);
	
	SEM_unlock(&semListeBot);
	continue;
      }
      else{
	bot = listeBot;
	node * bot_clean;
	while(bot->nextNode != NULL){
	  if((tmp_actuel - bot->nextNode->last_msg) > 10){
	    bot_clean = bot->nextNode;
	    bot->nextNode = bot_clean->nextNode->nextNode;

	    SEM_lock(&(bot_clean->SEM_socket));
	    if(bot_clean->fd != NULL){
	      fclose(bot_clean->fd);
	      bot_clean->fd = NULL;
	    }
	    SEM_unlock(&(bot_clean->SEM_socket));

	
	    free(bot_clean);
	  }
	  else{
	    bot = bot->nextNode;
	  }
	}
      }
    }
    
    SEM_unlock(&semListeBot);
    sleep(1);
  }
}

void cleanListeBot(node * liste){
  if(liste != NULL){
    cleanListeBot(liste->nextNode);
    free(liste);
  }
}

FILE * prendreSocket(node * bot){
  SEM_lock(&semListeBot);
  FILE * fd;
  if( bot->fd == NULL){
    int s = connexionServeurTCP(bot->hote,portTCP);
    fd = fdopen(s,"a+");
    if(fd==NULL){ perror("gestionClient.fdopen"); exit(EXIT_FAILURE); }
    bot->fd = fd;
    SEM_init(&(bot->SEM_socket));
  }
  SEM_lock(&(bot->SEM_socket));
  SEM_unlock(&semListeBot);
  return bot->fd;
}

void rendreSocket(node * bot){
  SEM_lock(&semListeBot);
  SEM_unlock(&(bot->SEM_socket));
  SEM_unlock(&semListeBot); 
}


void fonctionUDP(void * arg){
  argumentUDP params = *(argumentUDP *)arg;
  boucleServeurUDP(params.socket,fonction);
}

void sendResultSHM(int size, char * result){
  int SHMidSize;
  while((SHMidSize = SHM_create(SHMkeySizeResult,sizeof(int))) == -1){ // remplacer int
    printf("Size: En attente de détachement ServeurWeb\n");
    sleep(1);
  }

  int * pt_mem_size = SHM_write_int(SHMidSize,size);

  SHM_detach_int(&pt_mem_size);

  int SHMidResult;

  while((SHMidResult = SHM_create(SHMkeyResult,size)) == -1){
    printf("File: En attente de détachement du candc\n");
    sleep(1);
  } 

  char * pt_mem_char = SHM_write_char(SHMidResult, "", strlen(""));
  pt_mem_char = SHM_write_char(SHMidResult, result, size);

  SHM_detach_char(&pt_mem_char);
}

void Action_lancerCharge(void * arg){
  argumentsAction param = *(argumentsAction *)arg;
    
  FILE *dialogue= prendreSocket(param.bot);

  char ligne[MAX_TAMPON] = "";
  //commande
  fprintf(dialogue,"execute?\n");
  fgets(ligne,MAX_TAMPON,dialogue);
  if(strcmp(ligne,"OKexecute\n") != 0){
    printf("%s\n",ligne);
    char texteAEnvoyer[TAILLE_MSG_MAX] = "";
    sprintf(texteAEnvoyer,"Erreur sur la réponse du bot\n");
    FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);
    FDM_write(param.FDMreponse, param.FDMtype, "end");
    rendreSocket(param.bot);
    return;
  }
  //taille charge
  fprintf(dialogue,"%ld\n",strlen(param.charge));
  fgets(ligne,MAX_TAMPON,dialogue);
  if(strcmp(ligne,"OKname_size\n") != 0){
    printf("%s\n",ligne);
    char texteAEnvoyer[TAILLE_MSG_MAX] = "";
    sprintf(texteAEnvoyer,"Erreur sur la réponse du bot\n");
    FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);
    FDM_write(param.FDMreponse, param.FDMtype, "end");
    rendreSocket(param.bot);
    return;
  }
  //charge
  fprintf(dialogue,"%s\n",param.charge);
  fgets(ligne,MAX_TAMPON,dialogue);
  if(strcmp(ligne,"OKname\n") != 0){
    printf("%s\n",ligne);
    char texteAEnvoyer[TAILLE_MSG_MAX] = "";
    sprintf(texteAEnvoyer,"Erreur sur la réponse du bot\n");
    FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);
    FDM_write(param.FDMreponse, param.FDMtype, "end");
    rendreSocket(param.bot);
    return;
  }
  //recup id
  int id_exec;
  fgets(ligne,MAX_TAMPON,dialogue);
  sscanf(ligne,"%d",&id_exec);

  fprintf(dialogue,"OKid\n");
  //bye
  fgets(ligne,MAX_TAMPON,dialogue);
  if(strcmp(ligne,"bye\n") != 0){
    printf("%s\n",ligne);
    char texteAEnvoyer[TAILLE_MSG_MAX] = "";
    sprintf(texteAEnvoyer,"Erreur sur la réponse du bot\n");
    FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);
    FDM_write(param.FDMreponse, param.FDMtype, "end");
    rendreSocket(param.bot);
    return;
  }

  fprintf(dialogue,"bye\n");

  char texteAEnvoyer[TAILLE_MSG_MAX*2] = "";
  sprintf(texteAEnvoyer,"Bot: %s, Id d'execution numéro: %d",param.bot->id,id_exec);
  if(param.isWeb == 0){
    FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);
    FDM_write(param.FDMreponse, param.FDMtype, "end");
  }
  if(param.isWeb == 1){
    sendResultSHM(strlen(texteAEnvoyer),texteAEnvoyer);
  }

  rendreSocket(param.bot);
}

void Action_uploadCharge(void * arg){
  argumentsAction param = *(argumentsAction *)arg;

  if(param.taille_file > 0){
    
    FILE *dialogue= prendreSocket(param.bot);
  
    //commande
    char ligne[MAX_TAMPON] = "";
    fprintf(dialogue,"upload?\n");
    fgets(ligne,MAX_TAMPON,dialogue);
    if(strcmp(ligne,"OKupload\n") != 0){
      char texteAEnvoyer[TAILLE_MSG_MAX] = "";
      sprintf(texteAEnvoyer,"Erreur sur la réponse du bot\n");
      FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);
      FDM_write(param.FDMreponse, param.FDMtype, "end");
      rendreSocket(param.bot);
      return;
    }
    //taille nom
    fprintf(dialogue,"%d",param.taille_nom);
    fprintf(dialogue,"\n");
    fgets(ligne,MAX_TAMPON,dialogue);
    if(strcmp(ligne,"OKname_size\n") != 0){
      char texteAEnvoyer[TAILLE_MSG_MAX] = "";
      sprintf(texteAEnvoyer,"Erreur sur la réponse du bot\n");
      FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);
      FDM_write(param.FDMreponse, param.FDMtype, "end");
      rendreSocket(param.bot);
      return;
    }
    //nom
    fprintf(dialogue,"%s",param.nom_file);
    fprintf(dialogue,"\n");
    fgets(ligne,MAX_TAMPON,dialogue);
    if(strcmp(ligne,"OKname\n") != 0){
      char texteAEnvoyer[TAILLE_MSG_MAX] = "";
      sprintf(texteAEnvoyer,"Erreur sur la réponse du bot\n");
      FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);
      FDM_write(param.FDMreponse, param.FDMtype, "end");
      rendreSocket(param.bot);
      return;
    }
    //taille data
    fprintf(dialogue,"%d",param.taille_file);
    fprintf(dialogue,"\n");
    fgets(ligne,MAX_TAMPON,dialogue);
    if(strcmp(ligne,"OKdata_size\n") != 0){
      char texteAEnvoyer[TAILLE_MSG_MAX] = "";
      sprintf(texteAEnvoyer,"Erreur sur la réponse du bot\n");
      FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);
      FDM_write(param.FDMreponse, param.FDMtype, "end");
      rendreSocket(param.bot);
      return;
    }
    //data
    unsigned char * pt_file = param.file;
    for(int i = 0; i < param.taille_file; i++){
      fwrite(pt_file,1,1,dialogue);
      pt_file ++;
    }
    fgets(ligne,MAX_TAMPON,dialogue);
    if(strcmp(ligne,"bye\n") != 0){
      char texteAEnvoyer[TAILLE_MSG_MAX] = "";
      sprintf(texteAEnvoyer,"Erreur sur la réponse du bot\n");
      FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);
      FDM_write(param.FDMreponse, param.FDMtype, "end");
      rendreSocket(param.bot);
      return;
    }
    fprintf(dialogue,"bye\n");

    //reponse admin
    char texteAEnvoyer[TAILLE_MSG_MAX] = "";
    sprintf(texteAEnvoyer,"Charge en cours d'installation");
    if(param.isWeb == 0){
      FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);
      FDM_write(param.FDMreponse, param.FDMtype, "end");
    }
    if(param.isWeb == 1){
      sendResultSHM(strlen(texteAEnvoyer),texteAEnvoyer);
    }

    rendreSocket(param.bot);
  }
  else{
    char texteAEnvoyer[TAILLE_MSG_MAX] = "Il n'y a pas de charge à upload!";
    if(param.isWeb == 0){
      FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);
      FDM_write(param.FDMreponse, param.FDMtype, "end");
    }
    if(param.isWeb == 1){
      sendResultSHM(strlen(texteAEnvoyer),texteAEnvoyer);
    }
  }
    
}

void Action_deletCharge(void * arg){
  argumentsAction param = *(argumentsAction *)arg;

  FILE *dialogue= prendreSocket(param.bot);

  char ligne[MAX_TAMPON] = "";
  //commande
  fprintf(dialogue,"delet?\n");
  fgets(ligne,MAX_TAMPON,dialogue);
  if(strcmp(ligne,"OKdelet\n") != 0){
    char texteAEnvoyer[TAILLE_MSG_MAX] = "";
    sprintf(texteAEnvoyer,"Erreur sur la réponse du bot\n");
    FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);
    FDM_write(param.FDMreponse, param.FDMtype, "end");
    rendreSocket(param.bot);
    return;
  }
  //taille charge
  fprintf(dialogue,"%ld\n",strlen(param.charge));
  fgets(ligne,MAX_TAMPON,dialogue);
  if(strcmp(ligne,"OKname_size\n") != 0){
    char texteAEnvoyer[TAILLE_MSG_MAX] = "";
    sprintf(texteAEnvoyer,"Erreur sur la réponse du bot\n");
    FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);
    FDM_write(param.FDMreponse, param.FDMtype, "end");
    rendreSocket(param.bot);
    return;
  }
  //charge
  fprintf(dialogue,"%s\n",param.charge);
  fgets(ligne,MAX_TAMPON,dialogue);
  if(strcmp(ligne,"OKname\n") != 0){
    char texteAEnvoyer[TAILLE_MSG_MAX] = "";
    sprintf(texteAEnvoyer,"Erreur sur la réponse du bot\n");
    FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);
    FDM_write(param.FDMreponse, param.FDMtype, "end");
    rendreSocket(param.bot);
    return;
  }
    
  //bye
  fprintf(dialogue,"bye\n");
  fgets(ligne,MAX_TAMPON,dialogue);
  if(strcmp(ligne,"bye\n") != 0){
    char texteAEnvoyer[TAILLE_MSG_MAX] = "";
    sprintf(texteAEnvoyer,"Erreur sur la réponse du bot\n");
    FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);
    FDM_write(param.FDMreponse, param.FDMtype, "end");
    rendreSocket(param.bot);
    return;
  }

  //reponse admin
  char texteAEnvoyer[TAILLE_MSG_MAX] = "";
  sprintf(texteAEnvoyer,"Charge en cours de suppression");
  if(param.isWeb == 0){
    FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);
    FDM_write(param.FDMreponse, param.FDMtype, "end");
  }
  if(param.isWeb == 1){
    sendResultSHM(strlen(texteAEnvoyer),texteAEnvoyer);
  }
    
  rendreSocket(param.bot);
    
}

void Action_etatBot(void * arg){
  argumentsAction param = *(argumentsAction *)arg;

  FILE *dialogue= prendreSocket(param.bot);

  char ligne[MAX_TAMPON] = "";

  int nbExec, nbCharge;
  //commande
  fprintf(dialogue,"state?\n");
  //nb charge
  fgets(ligne,MAX_TAMPON,dialogue);
  sscanf(ligne,"%d",&nbCharge);
  fprintf(dialogue,"OKnbCharge\n");
  //nb execution
  fgets(ligne,MAX_TAMPON,dialogue);
  sscanf(ligne,"%d",&nbExec);
  fprintf(dialogue,"OKnbExecs\n");
  //bye
  fgets(ligne,MAX_TAMPON,dialogue);
  if(strcmp(ligne,"bye\n") != 0){
    char texteAEnvoyer[TAILLE_MSG_MAX] = "";
    sprintf(texteAEnvoyer,"Erreur sur la réponse du bot\n");
    FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);
    FDM_write(param.FDMreponse, param.FDMtype, "end");
    rendreSocket(param.bot);
    return;
  }
  fprintf(dialogue,"bye\n");

  //reponse admin
  char texteAEnvoyer[TAILLE_MSG_MAX] = "";
  sprintf(texteAEnvoyer,"Nombre de charge installée: %d Nombre de charge executée: %d",nbCharge,nbExec);
  if(param.isWeb == 0){
    FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer); 
    FDM_write(param.FDMreponse, param.FDMtype, "end");
  }
  if(param.isWeb == 1){
    sendResultSHM(strlen(texteAEnvoyer),texteAEnvoyer);
  }
    
  rendreSocket(param.bot);
    
}

void Action_etatBot2(void * arg){
  argumentsAction param = *(argumentsAction *)arg;

  FILE *dialogue= prendreSocket(param.bot);

  char ligne[MAX_TAMPON] = "";

  int nbExec, nbCharge, tmp;
  //commande
  fprintf(dialogue,"STAT\n");
  //nb charge
  fgets(ligne,MAX_TAMPON,dialogue);
  sscanf(ligne,"%d,%d,%d",&tmp,&nbCharge,&nbExec);
  //reponse admin
  char texteAEnvoyer[TAILLE_MSG_MAX] = "";
  sprintf(texteAEnvoyer,"Nombre de charge installée: %d Nombre de charge executée: %d Temps de vie: %d",nbCharge,nbExec,tmp);
  if(param.isWeb == 0){
    FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer); 
    FDM_write(param.FDMreponse, param.FDMtype, "end");
  }
  if(param.isWeb == 1){
    sendResultSHM(strlen(texteAEnvoyer),texteAEnvoyer);
  }
    
  rendreSocket(param.bot);
    
}

void Action_resultatBot(void * arg){
  argumentsAction param = *(argumentsAction *)arg;

  FILE *dialogue= prendreSocket(param.bot);

  char ligne[MAX_TAMPON] = "";

  //commande
  fprintf(dialogue,"result?\n");
  fgets(ligne,MAX_TAMPON,dialogue);
  if(strcmp(ligne,"OKresult\n") != 0){
    perror("Erreur sur la réponse du bot");
    exit(1);
  }
  //id
  fprintf(dialogue,"%d\n",param.idExecution);
  fgets(ligne,MAX_TAMPON,dialogue);
  if(strcmp(ligne,"OKid\n") != 0){
    //regarder pour NOKid
    if(strcmp(ligne,"NOKid\n") == 0){
      char texteAEnvoyer[TAILLE_MSG_MAX] = "";
      sprintf(texteAEnvoyer,"Le bot n'a pas trouvé cette id!\n");
      FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);
      FDM_write(param.FDMreponse, param.FDMtype, "end");
      rendreSocket(param.bot);
      return;
    }
    else{
      char texteAEnvoyer[TAILLE_MSG_MAX] = "";
      sprintf(texteAEnvoyer,"Erreur sur la réponse du bot\n");
      FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);
      FDM_write(param.FDMreponse, param.FDMtype, "end");
      rendreSocket(param.bot);
      return;
    }
  }
  //taille result
  int taille_result;
  fgets(ligne,MAX_TAMPON,dialogue);
  sscanf(ligne,"%d",&taille_result);
  fprintf(dialogue,"OKresult_size\n");
  //result
  char * result, * pt_result;
  result = malloc(taille_result);
  pt_result = result;
  for(int i = 0; i < taille_result; i++){
    fread(pt_result,1,1,dialogue);
    //fwrite(pt_result,1,1,stdout);
    pt_result ++;
  }
  //bye
  fprintf(dialogue,"bye\n");
  fgets(ligne,MAX_TAMPON,dialogue);
  if(strcmp(ligne,"bye\n") != 0){
    char texteAEnvoyer[TAILLE_MSG_MAX] = "";
    sprintf(texteAEnvoyer,"Erreur sur la réponse du bot\n");
    FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);
    FDM_write(param.FDMreponse, param.FDMtype, "end");
    rendreSocket(param.bot);
    return;
  }

  //reponse admin
  if(param.isWeb == 0){
    int nbMessage = taille_result / TAILLE_MSG_MAX + 1;
    char texteAEnvoyer[TAILLE_MSG_MAX] = "";
    pt_result = result;
    /*sprintf(texteAEnvoyer,"%d",nbMessage);
      FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);*/
    for(int i = 0; i < nbMessage; i++){
      strncpy(texteAEnvoyer,pt_result,TAILLE_MSG_MAX);
      pt_result += TAILLE_MSG_MAX;
      FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);
    }

    FDM_write(param.FDMreponse, param.FDMtype, "end");
  }
  if(param.isWeb == 1){
      sendResultSHM(taille_result,result);
  }
    
  rendreSocket(param.bot);
    
}

void Action_downloadCharge(void * arg){
  argumentsAction param = *(argumentsAction *)arg;

  char texteAEnvoyer[TAILLE_MSG_MAX] = "";
  struct stat statFile;
  
  if(lstat(param.path,&statFile) != 0){
    sprintf(texteAEnvoyer,"Ce chemin n'existe pas.");
    FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);
    //printf("erreur\n");
  }
  else{
    if(!S_ISREG(statFile.st_mode)){
      sprintf(texteAEnvoyer,"Ce chemin ne mène pas vers un fichier.");
      FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);
    }
    else{
      sprintf(texteAEnvoyer,"Chargement du fichier en cours.");
      FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);

      char * name, * next, * save_path;
      name = malloc(strlen(param.path));
      save_path = malloc(strlen(param.path));
      strcpy(save_path,param.path);
      next = strtok(save_path, "/");
      strcpy(name,next);
      while((next = strtok(NULL,"/")) != NULL){
	strcpy(name,next);
      }

      int fd=open(param.path,O_RDONLY);
      if(fd>=0){

	SEM_lock(&semFile);
	if(nom_file != NULL){
	  free(nom_file);
	}
	nom_file = malloc(strlen(name));
	strcpy(nom_file,name);
	taille_nom = strlen(name);

	taille_file = statFile.st_size;
	if(file != NULL){
	  free(file);
	}
	file = malloc(taille_file);

	unsigned char * pt_file = file;
	for(int i = 0; i < taille_file; i++){
	  read(fd,pt_file,1);
	  pt_file ++;
	}
	
	SEM_unlock(&semFile);

	sprintf(texteAEnvoyer,"Chargement du fichier terminé.");
	FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);
      }
      else{
	sprintf(texteAEnvoyer,"Le fichier n'a pas pu être ouvert.");
	FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer);
      }

      close(fd);
      
      
    }
  }
  
  FDM_write(param.FDMreponse, param.FDMtype, "end");
    
}


void Action_quitBot(void * arg){
  argumentsAction param = *(argumentsAction *)arg;

  char texteAEnvoyer[TAILLE_MSG_MAX] = "";
  sprintf(texteAEnvoyer,"Pas de communication avec le bot");

  SEM_lock(&semListeBot);
  SEM_lock(&(param.bot->SEM_socket));
  if(param.bot->fd != NULL){
    fclose(param.bot->fd);
    param.bot->fd = NULL;
    sprintf(texteAEnvoyer,"Communication avec le bot fermé");
  }
  SEM_unlock(&(param.bot->SEM_socket));
  SEM_unlock(&semListeBot);

  //reponse admin
  
  if(param.isWeb == 0){
    FDM_write(param.FDMreponse, param.FDMtype, texteAEnvoyer); 
    FDM_write(param.FDMreponse, param.FDMtype, "end");
  }
  if(param.isWeb == 1){
    sendResultSHM(strlen(texteAEnvoyer),texteAEnvoyer);
  }
    
}

node * getInfoBot(char * id){
  node * bot = listeBot;
  int find = 0;

  SEM_lock(&semListeBot);
  while(bot != NULL){
    if((strcmp(bot->id,id) == 0) || (strcmp(bot->hote,id) == 0)){
      find = 1;
      break;
    }
    bot = bot->nextNode;
  }

  SEM_unlock(&semListeBot);
  
  if(find == 0){
    return NULL;
  }

  return bot;
}


int Init_lanceOuDeletCharge(argumentsAction * args,char * id, char * charge, int FDMreponse, int FDMtype){

  node * bot = getInfoBot(id);

  if(bot == NULL){
    return -1;
  }

  args->bot = bot;
  
  strcpy(args->ip,bot->hote);
  strcpy(args->charge,charge);

  args->FDMreponse = FDMreponse;
  args->FDMtype = FDMtype;

  args->isWeb = 0;
    
  return 0;
}

int Init_uploadCharge(argumentsAction * args,char * id, int FDMreponse, int FDMtype){
  node * bot = getInfoBot(id);

  if(bot == NULL){
    return -1;
  }

  args->bot = bot;

  //semaphore
  SEM_lock(&semFile);
  if(taille_file > 0){
    
    strcpy(args->ip,bot->hote);
    args->taille_file = taille_file;
    args->file = malloc(args->taille_file);
    memcpy(args->file,file,args->taille_file);
    args->taille_nom = taille_nom;
    args->nom_file = malloc(args->taille_nom);
    strcpy(args->nom_file,nom_file);
  }
  else{
    args->taille_file = -1;
  }
  SEM_unlock(&semFile);

  args->FDMreponse = FDMreponse;
  args->FDMtype = FDMtype;
  
  args->isWeb = 0;
    
  return 0;
}

int Init_etatBot(argumentsAction * args,char * id, int FDMreponse, int FDMtype){
  node * bot = getInfoBot(id);

  if(bot == NULL){
    return -1;
  }

  args->bot = bot;
  
  strcpy(args->ip,bot->hote);

  args->FDMreponse = FDMreponse;
  args->FDMtype = FDMtype;

  args->isWeb = 0;
    
    
  return 0;
}

int Init_resultatBot(argumentsAction * args,char * id, int idExec, int FDMreponse, int FDMtype){
  node * bot = getInfoBot(id);

  if(bot == NULL){
    return -1;
  }

  args->bot = bot;
  
  strcpy(args->ip,bot->hote);

  args->FDMreponse = FDMreponse;
  args->FDMtype = FDMtype;
  args->idExecution = idExec;

  args->isWeb = 0;
  
  return 0;
}

int Init_downloadCharge(argumentsAction * args,char * path, int FDMreponse, int FDMtype){

  args->path = malloc(strlen(path));
  strcpy(args->path, path);

  args->FDMreponse = FDMreponse;
  args->FDMtype = FDMtype;

  args->isWeb = 0;
  
  return 0;
}

int Init_quitBot(argumentsAction * args,char * id, int FDMreponse, int FDMtype){
  node * bot = getInfoBot(id);

  if(bot == NULL){
    return -1;
  }

  args->bot = bot;

  args->FDMreponse = FDMreponse;
  args->FDMtype = FDMtype;

  args->isWeb = 0;
  
  return 0;
}


void appliquerCommande(int FDMcommandeId, int FDMreponseId, int type, char * texte){
  if(type == TYPE_LISTE_BOT){
    SEM_lock(&semListeBot);
    node * bot = listeBot;
    if(bot == NULL){
      FDM_write(FDMreponseId, type, "Pas de bot en ligne");
    }
    else{
      char texteAEnvoyer[TAILLE_MSG_MAX];
      while(bot != NULL){
	char tmp[TAILLE_MSG_MAX];
	sprintf(tmp,"%d",bot->tmp_vie);
	strcpy(texteAEnvoyer,"");
	strcat(texteAEnvoyer,"Id bot: ");
	strcat(texteAEnvoyer,bot->id);
	strcat(texteAEnvoyer," Adresse IP: ");
	strcat(texteAEnvoyer,bot->hote);
	strcat(texteAEnvoyer," Temps de vie: ");
	strcat(texteAEnvoyer,tmp);
	FDM_write(FDMreponseId, type, texteAEnvoyer);
	bot = bot->nextNode;
      }
    }
    FDM_write(FDMreponseId, type, "end");
    SEM_unlock(&semListeBot);
  }
  else if (type == TYPE_ACTIVATION_CHARGE){
    argumentsAction args;
	
    FDMmsg MsgCharge;
    MsgCharge = FDM_read(FDMcommandeId,type);
    char * nomCharge = MsgCharge.texte;
    char * idBot;

    idBot = malloc(strlen(texte));
	
    strcpy(idBot,texte);
	
    if(Init_lanceOuDeletCharge(&args,idBot,nomCharge,FDMreponseId,type) == -1){
      FDM_write(FDMreponseId, type, "Pas de bot correspondant à celui demandé");
      FDM_write(FDMreponseId, type, "end");
    }
    else{
      lancerFlux(Action_lancerCharge,&args, sizeof(args));
    }
	
  }
  else if (type == TYPE_UPLOAD_CHARGE){
    argumentsAction args;
	
    char * idBot;

    idBot = malloc(strlen(texte));
	
    strcpy(idBot,texte);
	
    if(Init_uploadCharge(&args,idBot,FDMreponseId,type) == -1){
      FDM_write(FDMreponseId, type, "Pas de bot correspondant à celui demandé");
      FDM_write(FDMreponseId, type, "end");
    }
    else{
      lancerFlux(Action_uploadCharge,&args, sizeof(args));
    }
	
  }
  else if (type == TYPE_DELET_CHARGE){
    argumentsAction args;
	
    FDMmsg MsgCharge;
    MsgCharge = FDM_read(FDMcommandeId,type);
    char * nomCharge = MsgCharge.texte;
    char * idBot;

    idBot = malloc(strlen(texte));
	
    strcpy(idBot,texte);
	
    if(Init_lanceOuDeletCharge(&args,idBot,nomCharge,FDMreponseId,type) == -1){
      FDM_write(FDMreponseId, type, "Pas de bot correspondant à celui demandé");
      FDM_write(FDMreponseId, type, "end");
    }
    else{
      lancerFlux(Action_deletCharge,&args, sizeof(args));
    }
  }
  else if (type == TYPE_ETAT_BOT){
    argumentsAction args;
	
    char * idBot;

    idBot = malloc(strlen(texte));
	
    strcpy(idBot,texte);
	
    if(Init_etatBot(&args,idBot,FDMreponseId,type) == -1){
      FDM_write(FDMreponseId, type, "Pas de bot correspondant à celui demandé");
      FDM_write(FDMreponseId, type, "end");
    }
    else{
      lancerFlux(Action_etatBot2,&args, sizeof(args));
    }
  }
  else if (type == TYPE_RESULTAT_BOT){
    argumentsAction args;

    FDMmsg MsgId;
    MsgId = FDM_read(FDMcommandeId,type);
    char * idBot;
    int idExec = atoi(MsgId.texte);

    idBot = malloc(strlen(texte));
	
    strcpy(idBot,texte);
	
    if(Init_resultatBot(&args,idBot,idExec,FDMreponseId,type) == -1){
      FDM_write(FDMreponseId, type, "Pas de bot correspondant à celui demandé");
      FDM_write(FDMreponseId, type, "end");
    }
    else{
      lancerFlux(Action_resultatBot,&args, sizeof(args));
    }
  }
  else if (type == TYPE_DOWNLOAD_CHARGE){
    argumentsAction args;

    char * path;

    path = malloc(strlen(texte));
	
    strcpy(path,texte);
	
    Init_downloadCharge(&args,path,FDMreponseId,type);

    free(path);

    lancerFlux(Action_downloadCharge,&args, sizeof(args));
  }
  else if (type == TYPE_QUIT_BOT){
    argumentsAction args;

    char * idBot;
    idBot = malloc(strlen(texte));
    strcpy(idBot,texte);
    
	
    if(Init_quitBot(&args,idBot,FDMreponseId,type) == -1){
      FDM_write(FDMreponseId, type, "Pas de bot correspondant à celui demandé");
      FDM_write(FDMreponseId, type, "end");
    }
    else{
      lancerFlux(Action_quitBot,&args, sizeof(args));
    }
  }
  else if (type == TYPE_ACTIVATION_LISTE_CHARGE){
    int nbParam = atoi(texte);

    FDMmsg MsgId;
    char * idBot = NULL;
    char * nomCharge = NULL;

    MsgId = FDM_read(FDMcommandeId,type);
    nomCharge = malloc(strlen(MsgId.texte));
    strcpy(nomCharge,MsgId.texte);

    for(int i = 0; i < nbParam; i++){
      argumentsAction args;
      
      MsgId = FDM_read(FDMcommandeId,type);
      if(idBot != NULL){
	free(idBot);
      }
      idBot = malloc(strlen(MsgId.texte));
      strcpy(idBot, MsgId.texte);
      
      if(Init_lanceOuDeletCharge(&args,idBot,nomCharge,FDMreponseId,type) == -1){
	FDM_write(FDMreponseId, type, "Pas de bot correspondant à celui demandé");
	FDM_write(FDMreponseId, type, "end");
      }
      else{
	lancerFlux(Action_lancerCharge,&args, sizeof(args));
      }

    }

    //FDM_write(FDMreponseId, type, "end");
    
  }
    
}

int getDataFromSHM(int SHMkeySize, int SHMkeyData, char ** data){
  int SHMidSize = SHM_get(SHMkeySize,sizeof(int));
  if(SHMidSize == -1){
    return -1;
  }
  int * pt_size;
  pt_size = SHM_read_int(SHMidSize);
  
  * data = malloc(*pt_size);
  int SHMidData = SHM_get(SHMkeyData,*pt_size);

  char * pt_data;
  SHM_read_char(SHMidData, &pt_data);
  strcpy(* data,pt_data);
  
  SHM_remove_char(SHMidData,&pt_data);
  SHM_remove_int(SHMidSize,&pt_size);

  return 0;
}

int InitWeb_lanceOuDeletCharge(argumentsAction * args){
  char * id = NULL;
  char * charge = NULL;

  getDataFromSHM(SHMkeySizeParam1, SHMkeyParam1, &id);
  getDataFromSHM(SHMkeySizeParam2, SHMkeyParam2, &charge);

  node * bot = getInfoBot(id);

  if(bot == NULL){
    return -1;
  }

  args->bot = bot;
  
  strcpy(args->ip,bot->hote);
  strcpy(args->charge,charge);

  args->isWeb = 1;

  free(id);
  free(charge);
    
  return 0;
}

int InitWeb_uploadCharge(argumentsAction * args){
  char * id = NULL;

  getDataFromSHM(SHMkeySizeParam1, SHMkeyParam1, &id);

  node * bot = getInfoBot(id);

  if(bot == NULL){
    return -1;
  }

  args->bot = bot;

  //semaphore
  SEM_lock(&semFile);
  if(taille_file > 0){
    
    strcpy(args->ip,bot->hote);
    args->taille_file = taille_file;
    args->file = malloc(args->taille_file);
    memcpy(args->file,file,args->taille_file);
    args->taille_nom = taille_nom;
    args->nom_file = malloc(args->taille_nom);
    strcpy(args->nom_file,nom_file);
  }
  else{
    args->taille_file = -1;
  }
  SEM_unlock(&semFile);
  
  args->isWeb = 1;

  free(id);
    
  return 0;
}

int InitWeb_etatBot(argumentsAction * args){
  char * id = NULL;

  getDataFromSHM(SHMkeySizeParam1, SHMkeyParam1, &id);

  node * bot = getInfoBot(id);

  if(bot == NULL){
    return -1;
  }

  args->bot = bot;
  
  strcpy(args->ip,bot->hote);


  args->isWeb = 1;
    
  free(id);
  
  return 0;
}

int InitWeb_resultatBot(argumentsAction * args){
  char * id = NULL;
  char * idExec = NULL;

  getDataFromSHM(SHMkeySizeParam1, SHMkeyParam1, &id);
  getDataFromSHM(SHMkeySizeParam2, SHMkeyParam2, &idExec);

  node * bot = getInfoBot(id);

  if(bot == NULL){
    return -1;
  }

  args->bot = bot;

  strcpy(args->ip,bot->hote);

  args->idExecution = atoi(idExec);

  args->isWeb = 1;

  free(id);
  free(idExec);
  
  return 0;
}

int InitWeb_quitBot(argumentsAction * args){
  char * id = NULL;

  getDataFromSHM(SHMkeySizeParam1, SHMkeyParam1, &id);

 node * bot = getInfoBot(id);

  if(bot == NULL){
    return -1;
  }

  args->bot = bot;

  strcpy(args->ip,bot->hote);

  args->isWeb = 1;

  free(id);
  
  return 0;
}

int InitWeb_lanceListeCharge(argumentsAction * args){
  char * id = NULL;

  if(getDataFromSHM(SHMkeySizeParam2, SHMkeyParam2, &id) == -1){
    return -2;
  }

  node * bot = getInfoBot(id);

  if(bot == NULL){
    return -1;
  }

  args->bot = bot;
  
  strcpy(args->ip,bot->hote);

  args->isWeb = 1;

  free(id);
    
  return 0;
}

int InitWeb_recupCharge(argumentsAction * args){
  char * charge = NULL;

  if(getDataFromSHM(SHMkeySizeParam1, SHMkeyParam1, &charge) == -1){
    return -2;
  }

  strcpy(args->charge,charge);

  free(charge);
    
  return 0;
}

void appliquerCommandeWeb(void * arg){
  int SHMidCmd = SHM_get(SHMkeyCmd,sizeof(int));
  int * pt_cmd;
  int cmd;
  pt_cmd = SHM_read_int(SHMidCmd);
  cmd = * pt_cmd;
  SHM_remove_int(SHMidCmd,&pt_cmd);
  
  if(cmd == TYPE_LISTE_BOT){
    SEM_lock(&semListeBot);
    node * bot = listeBot;
    if(bot == NULL){
      sendResultSHM(strlen("Pas de bot en ligne"),"Pas de bot en ligne");
    }
    else{
      int nbBot = 0;
      while(bot != NULL){
	nbBot ++;
	bot = bot->nextNode;
      }
      char * result;
      char textId[] = "Id bot: ";
      char textIP[] = " Adresse IP: ";
      char textEnd[] = "<br>";
      result = malloc((MAX_TAMPON * 2 + strlen(textId) + strlen(textIP) + strlen(textEnd)) * nbBot);
      bot = listeBot;
      while(bot != NULL){
	strcat(result,textEnd);
	strcat(result,textId);
	strcat(result,bot->id);
	strcat(result,textIP);
	strcat(result,bot->hote);
	bot = bot->nextNode;
      }
      sendResultSHM(strlen(result),result);
      
    }
    SEM_unlock(&semListeBot);
  }
  else if (cmd == TYPE_ACTIVATION_CHARGE){
    argumentsAction args;

    if(InitWeb_lanceOuDeletCharge(&args) == -1){
      sendResultSHM(strlen("Le bot n'est pas répertorié"),"Le bot n'est pas répertorié");
      return;
    }

    lancerFlux(Action_lancerCharge,&args, sizeof(args));
	
  }
  else if (cmd == TYPE_UPLOAD_CHARGE){
    argumentsAction args;
	
    if(InitWeb_uploadCharge(&args) == -1){
      sendResultSHM(strlen("Le bot n'est pas répertorié"),"Le bot n'est pas répertorié");
      return;
    }

    lancerFlux(Action_uploadCharge,&args, sizeof(args));
	
  }
  else if (cmd == TYPE_DELET_CHARGE){
    argumentsAction args;
	
    if(InitWeb_lanceOuDeletCharge(&args) == -1){
      sendResultSHM(strlen("Le bot n'est pas répertorié"),"Le bot n'est pas répertorié");
      return;
    }

    lancerFlux(Action_deletCharge,&args, sizeof(args));
  }
  else if (cmd == TYPE_ETAT_BOT){
    argumentsAction args;
	
    if(InitWeb_etatBot(&args) == -1){
      sendResultSHM(strlen("Le bot n'est pas répertorié"),"Le bot n'est pas répertorié");
      return;
    }

    lancerFlux(Action_etatBot2,&args, sizeof(args));
  }
  else if (cmd == TYPE_RESULTAT_BOT){
    argumentsAction args;
	
    if(InitWeb_resultatBot(&args) == -1){
      sendResultSHM(strlen("Le bot n'est pas répertorié"),"Le bot n'est pas répertorié");
      return;
    }

    lancerFlux(Action_resultatBot,&args, sizeof(args));
  }

  else if (cmd == TYPE_QUIT_BOT){
    argumentsAction args;
	
    if(InitWeb_quitBot(&args) == -1){
      sendResultSHM(strlen("Le bot n'est pas répertorié"),"Le bot n'est pas répertorié");
      return;
    }

    lancerFlux(Action_quitBot,&args, sizeof(args));
  }
  else if (cmd == TYPE_ACTIVATION_LISTE_CHARGE){
    argumentsAction args;

    int retour = 0 ;

    InitWeb_recupCharge(&args);

    while((retour = InitWeb_lanceListeCharge(&args)) != -2){
      if(retour == -1){
	sendResultSHM(strlen("Le bot n'est pas répertorié"),"Le bot n'est pas répertorié");
	//return;
      }
      else{
	lancerFlux(Action_lancerCharge,&args, sizeof(args));
      }
      printf("lancé flux\n");
    }
    printf("fini\n");
    //sendResultSHM(strlen("Le bot n'est pas répertorié"),"Le bot n'est pas répertorié");
	
  }
  
}

void recuperationFichier(){
  SEM_lock(&semFile);
  //taille file
  int SHMidSizeFile = SHM_get(SHMkeySizeFile,sizeof(int));
  int * pt_taille_file; 
  pt_taille_file = SHM_read_int(SHMidSizeFile);
  taille_file = * pt_taille_file;
  //printf("taille file %d\n",taille_file);
  SHM_remove_int(SHMidSizeFile,&pt_taille_file);

  //file
  unsigned char * pt_file;
  int SHMidFile = SHM_get(SHMkeyFile, taille_file);
  SHM_read_uchar(SHMidFile, &pt_file);
  if(file != NULL){
    free(file);
  }
  file = malloc(taille_file);
  memcpy(file,pt_file,taille_file);
  SHM_remove_uchar(SHMidFile,&pt_file);
  pt_file = file;
  /* for(int i = 0 ; i< taille_file; i++){
     fwrite(pt_file,1,1,stdout);
     pt_file ++;
     }*/
  //taille nom
  int SHMidSizeName = SHM_get(SHMkeySizeName,sizeof(int));
  int * pt_taille_name; 
  pt_taille_name = SHM_read_int(SHMidSizeName);
  taille_nom = * pt_taille_name;
  //printf("taille nom %d\n",taille_nom);
  SHM_remove_int(SHMidSizeName,&pt_taille_name);
  //nom
  char * pt_name;
  int SHMidName = SHM_get(SHMkeyName, taille_nom);
  SHM_read_char(SHMidName, &pt_name);
  if(nom_file != NULL){
    free(nom_file);
  }
  nom_file = malloc(taille_nom);
  strcpy(nom_file,pt_name);
  SHM_remove_char(SHMidName,&pt_name);
  //printf("%s\n",nom_file);
  printf("fichier recu\n");
  SEM_unlock(&semFile);
  sendResultSHM(strlen("Chargement du fichier terminé"),"Chargement du fichier terminé");
}
 
//Gestion des interruptions
void handlerFDM1(int sig){
  if(sig == SIGINT){
    printf("fermeture\n");
    FDM_close(FDM_get(FDM1key));
    shutdown(socketTCP,SHUT_RDWR);
    fermeSocketClientUDP(socketUDP,handleUDP);
    cleanListeBot(listeBot);
    free(file);
    exit(SIGINT);
  }
  else if (sig==SIGUSR1){
    recuperationFichier();
    struct sigaction action;
    action.sa_handler = handlerFDM1;
    sigaction(SIGUSR1, &action, NULL);
  }
  else if (sig==SIGUSR2){
    lancerFlux(appliquerCommandeWeb,NULL,0);
    struct sigaction action;
    action.sa_handler = handlerFDM1;
    sigaction(SIGUSR2, &action, NULL);
  }
}

//Init gestion des interruptions
void initHandlerForInterruption(void){
  struct sigaction action;
  action.sa_handler = handlerFDM1;
  sigaction(SIGINT,&action,NULL);
  sigaction(SIGUSR1, &action, NULL);
  sigaction(SIGUSR2, &action, NULL);
}
 
int main(void){

  SEM_init(&semFile);

  //mise en mémoire partagé du pid pour le ServeurWeb
  pid_t pid = getpid();
    
  int SHMidPid = SHM_create(SHMkeyPIDCandC,sizeof(pid));
  if(SHMidPid == -1){
    printf("Override mémoire partagé pour le PID\n");
    SHMidPid = SHM_get(SHMkeyPIDCandC,sizeof(pid));
    //exit(1);
  }  
     
  pid_t * pt_mem_pid = SHM_write_pid(SHMidPid,pid); 

  SHM_detach_pid(&pt_mem_pid);

    
  //lancement UDP
  socketUDP = initialisationSocketUDP(portUDP);

  argumentUDP arg;
  arg.socket = socketUDP;
  lancerFlux(fonctionUDP,&arg,sizeof(arg));

  //inti semaphore liste bot
  SEM_init(&semListeBot);

  lancerFlux(cleanBotAFK,NULL,0);
    
  int FDM1id = FDM_create(FDM1key); //creation FDM pour recevoir les commandes
  initHandlerForInterruption();
  while(1){
    //Lecture premier message
    FDMmsg rcv1 = FDM_read(FDM1id, TYPE_LIRE_PROCHAIN);
    if(rcv1.type != TYPE_DEMANDE_CONNEXION) continue; //si message autre que demande de connexion on en tient pas compte
    //Connexion a FDM2
    int FDM2id = atoi(rcv1.texte);
    //printf("FDM2id = %d\n", FDM2id);

    FDM_write(FDM2id, TYPE_VALIDATION_CONNEXION , "ok"); //retour "connexion etablie"
    FDMmsg rcv2 = FDM_read(FDM1id, TYPE_LIRE_PROCHAIN); //lecture de la commande

    //Traitement
    //printf("Commande recue: %ld, %s\n",rcv2.type, rcv2.texte);

    appliquerCommande(FDM1id, FDM2id, rcv2.type, rcv2.texte);
 
    FDM_write(FDM2id, (rcv2.type+6), "ok");
  }


  FDM_close(FDM1id);
  fermeSocketClientUDP(socketUDP,handleUDP);

  return 0;
}
