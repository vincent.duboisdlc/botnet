#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <string.h>
#include <signal.h>
#include <time.h>

#include "../IPC/libIPC.h"

int FDM2id;

void envoieCommande(int FDMid, int type, int  argc, char ** argv){ //cf protocole pour chaque commande
  if(type == TYPE_LISTE_BOT){
    if(argc == 2){
      FDM_write(FDMid, type, "");
    }
    else{
      perror("Nombre d'argument différent");
      exit(1);
    }
  }
  else if(type == TYPE_ACTIVATION_CHARGE){
    if(argc == 4){
      FDM_write(FDMid, type, argv[2]);
      FDM_write(FDMid, type, argv[3]);
    }
    else{
      perror("Nombre d'argument différent");
      exit(1);
    }
  }
  else if (type == TYPE_UPLOAD_CHARGE){
    if(argc == 3){
      FDM_write(FDMid, type, argv[2]);
    }
    else{
      perror("Nombre d'argument différent");
      exit(1);
    }
  }
  else if (type == TYPE_DELET_CHARGE){
    if(argc == 4){
      FDM_write(FDMid, type, argv[2]);
      FDM_write(FDMid, type, argv[3]);
    }
    else{
      perror("Nombre d'argument différent");
      exit(1);
    }
  }
  else if (type == TYPE_ETAT_BOT){
    if(argc == 3){
      FDM_write(FDMid, type, argv[2]);
    }
    else{
      perror("Nombre d'argument différent");
      exit(1);
    }
  }
  else if (type == TYPE_RESULTAT_BOT){
    if(argc == 4){
      FDM_write(FDMid, type, argv[2]);
      FDM_write(FDMid, type, argv[3]);
    }
    else{
      perror("Nombre d'argument différent");
      exit(1);
    }
  }
  else if (type == TYPE_DOWNLOAD_CHARGE){
    if(argc == 3){
      FDM_write(FDMid, type, argv[2]);
    }
    else{
      perror("Nombre d'argument différent");
      exit(1);
    }
  }
  else if (type == TYPE_QUIT_BOT){
    if(argc == 3){
      FDM_write(FDMid, type, argv[2]);
    }
    else{
      perror("Nombre d'argument différent");
      exit(1);
    }
  }
  else if (type == TYPE_ACTIVATION_LISTE_CHARGE){
    if(argc > 3){
      char nbId[TAILLE_MSG_MAX] = "";
      sprintf(nbId,"%d",argc-3);
      FDM_write(FDMid, type, nbId);
      for(int i = 0; i < argc-2 ;i++){
	FDM_write(FDMid, type, argv[i+2]);
      }
    }
    else{
      perror("Nombre d'argument différent");
      exit(1);
    }
  }
}

void lectureReponse(int type, int argc){
  if(type == TYPE_LISTE_BOT){
    FDMmsg reponse;
    do{
      reponse = FDM_read(FDM2id, type);
      if(strcmp(reponse.texte, "end") !=0 ){
	       printf("%s\n", reponse.texte);
      }
    } while(strcmp(reponse.texte,"end") != 0);
  }
  else if(type == TYPE_ACTIVATION_CHARGE){
    FDMmsg reponse;
    do{
      reponse = FDM_read(FDM2id,type);
      if(strcmp(reponse.texte,"end") !=0 ){
	       printf("%s\n",reponse.texte);
      }
    } while(strcmp(reponse.texte,"end") != 0);
  }
  else if(type == TYPE_UPLOAD_CHARGE){
    FDMmsg reponse;
    do{
      reponse = FDM_read(FDM2id,type);
      if(strcmp(reponse.texte,"end") !=0 ){
	       printf("%s\n",reponse.texte);
      }
    } while(strcmp(reponse.texte,"end") != 0);
  }
  else if(type == TYPE_DELET_CHARGE){
    FDMmsg reponse;
    do{
      reponse = FDM_read(FDM2id,type);
      if(strcmp(reponse.texte,"end") !=0 ){
	       printf("%s\n",reponse.texte);
      }
    } while(strcmp(reponse.texte,"end") != 0);
  }
  else if(type == TYPE_ETAT_BOT){
    FDMmsg reponse;
    do{
      reponse = FDM_read(FDM2id,type);
      if(strcmp(reponse.texte,"end") !=0 ){
	       printf("%s\n",reponse.texte);
      }
    } while(strcmp(reponse.texte,"end") != 0);
  }
  else if(type == TYPE_RESULTAT_BOT){
    FDMmsg reponse;
    printf("Résultat:\n");
    do{
      reponse = FDM_read(FDM2id,type);
      if(strcmp(reponse.texte,"end") !=0 ){
	       printf("%s",reponse.texte);
      }
    } while(strcmp(reponse.texte,"end") != 0);
  }
  else if(type == TYPE_DOWNLOAD_CHARGE){
    FDMmsg reponse;
    do{
      reponse = FDM_read(FDM2id,type);
      if(strcmp(reponse.texte,"end") !=0 ){
	       printf("%s\n",reponse.texte);
      }
    } while(strcmp(reponse.texte,"end") != 0);
  }
  else if(type == TYPE_QUIT_BOT){
    FDMmsg reponse;
    do{
      reponse = FDM_read(FDM2id,type);
      if(strcmp(reponse.texte,"end") !=0 ){
	       printf("%s\n",reponse.texte);
      }
    } while(strcmp(reponse.texte,"end") != 0);
  }
  else if(type == TYPE_ACTIVATION_LISTE_CHARGE){
    FDMmsg reponse;
    int nbEnd = 0;
    do{
      reponse = FDM_read(FDM2id,type);
      if(strcmp(reponse.texte,"end") !=0 ){
	       printf("%s\n",reponse.texte);
      }
      else{
	       nbEnd ++;
      }
    } while(nbEnd != (argc - 3));
  }
}

long transfoType(char ** argv,long type){
  if(strcmp(argv[1],"-l") == 0){
    return TYPE_LISTE_BOT;
  }
  else if(strcmp(argv[1],"-u") == 0){
    return TYPE_UPLOAD_CHARGE;
  }
  else if(strcmp(argv[1],"-d") == 0){
    return TYPE_DELET_CHARGE;
  }
  else if(strcmp(argv[1],"-r") == 0){
    return TYPE_RESULTAT_BOT;
  }
  else if(strcmp(argv[1],"-e") == 0){
    return TYPE_ETAT_BOT;
  }
  else if(strcmp(argv[1],"-a") == 0){
    return TYPE_ACTIVATION_CHARGE;
  }
  else if(strcmp(argv[1],"-dl") == 0){
    return TYPE_DOWNLOAD_CHARGE;
  }
  else if(strcmp(argv[1],"-q") == 0){
    return TYPE_QUIT_BOT;
  }
  else if(strcmp(argv[1],"-al") == 0){
    return TYPE_ACTIVATION_LISTE_CHARGE;
  }
  else{
    return type;
  }
}

//Gestion des interruptions
void handlerFDM2(int sig){
  if(sig == SIGINT){
    FDM_close(FDM2id);
    exit(SIGINT);
  }
}

//Init gestion des interruptions
void initHandlerForInterruption(void){
  struct sigaction action;
  action.sa_handler = handlerFDM2;
  sigaction(SIGINT,&action,NULL);
}

int main(int argc, char ** argv){
  //Params
  long MSGtype = (long)atoi(argv[1]); //type de commande
  MSGtype = transfoType(argv,MSGtype);

  int FDM1id = FDM_get(FDM1key); //se conecte a la file de message 1 pour envoyer des commandes au CandC

  FDM2id = FDM_create_private(); //creation de la file de message 2 pour recevoir les retours du CandC
  initHandlerForInterruption();

  char FDM2idChaine[TAILLE_MSG_MAX];
  sprintf(FDM2idChaine, "%d", FDM2id);
  FDM_write(FDM1id, 1, FDM2idChaine); //envoi l'id de la FDM2 au CandC
  FDMmsg rcv = FDM_read(FDM2id,TYPE_VALIDATION_CONNEXION); //retour de CandC valider la connexion
  if(strcmp(rcv.texte,"ok") != 0){
    perror("problème de validation du C&C\n");
    exit(1);
  }

  envoieCommande(FDM1id, MSGtype, argc, argv); //envoie commande

  lectureReponse(MSGtype,argc); //attente reponse pour fin action

  FDM_close(FDM2id); //fermeture fle de message 2

  return 0;
}
