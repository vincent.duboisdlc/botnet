#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>

#define portTCP "4242" 

int initialisationServeurTCP(char *service,int connexions);

int boucleServeurTCP(int ecoute,int (*traitement)(int));

int gestionClientTCP(int s);

int connexionServeurTCP(char *hote,char *service);
