#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>

#define portUDP "4242"

int initialisationSocketUDP(char *service);

int boucleServeurUDP(int s,int (*traitement)(unsigned char *,char *, int));

int creationSocketClientUDP(char *hote,char *service,void ** handle);

void fermeSocketClientUDP(int s, void * handle);

int envoieMessageUDP(int s, void * handle, unsigned char * message, int taille);
