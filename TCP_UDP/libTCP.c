#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>

#include <netinet/tcp.h>

#define MAX_LIGNE 10
#define MAX_UDP_MESSAGE 1

//serveur TCP
int initialisationServeurTCP(char *service,int connexions){
  struct addrinfo precisions,*resultat,*origine; // struct des sockets
  int statut;
  int s;

  /* Construction de la structure adresse */
  memset(&precisions,0,sizeof precisions); // resmise à zéro de &precision
  // definition des paramètres de la socket
  precisions.ai_family=AF_UNSPEC; //UNSPEC = on s'en fout de la famille
  precisions.ai_socktype=SOCK_STREAM; //socket unix
  precisions.ai_flags=AI_PASSIVE; //serveur d'écoute
  statut=getaddrinfo(NULL,service,&precisions,&origine); // recupérartion des socket disponible correspondant au paramètres rentrés
  if(statut<0){ perror("initialisationServeur.getaddrinfo"); exit(EXIT_FAILURE); }
  struct addrinfo *p;
  for(p=origine,resultat=origine;p!=NULL;p=p->ai_next)
    if(p->ai_family==AF_INET){ resultat=p; break; } // recherche d'une socket IPV6 dans les socket trouvées

  /* Creation d'une socket */
  s=socket(resultat->ai_family,resultat->ai_socktype,resultat->ai_protocol);
  if(s<0){ perror("initialisationServeur.socket"); exit(EXIT_FAILURE); }

  /* Options utiles */
  int vrai=1;
  if(setsockopt(s,SOL_SOCKET,SO_REUSEADDR,&vrai,sizeof(vrai))<0){
    perror("initialisationServeur.setsockopt (REUSEADDR)"); // pas de delay de réutilisation
    exit(EXIT_FAILURE);
  }
  if(setsockopt(s,IPPROTO_TCP,TCP_NODELAY,&vrai,sizeof(vrai))<0){
    perror("initialisationServeur.setsockopt (NODELAY)"); // pas de buffer 
    exit(EXIT_FAILURE);
  }

  /* Specification de l'adresse de la socket */
  statut=bind(s,resultat->ai_addr,resultat->ai_addrlen);
  if(statut<0) return -1;

  /* Liberation de la structure d'informations */
  freeaddrinfo(origine);

  /* Taille de la queue d'attente */
  statut=listen(s,connexions);
  if(statut<0) return -1;

  return s;
}

//gestion des clients TCP
int boucleServeurTCP(int ecoute,int (*traitement)(int))
{
  int dialogue;

  while(1){

    /* Attente d'une connexion */
    if((dialogue=accept(ecoute,NULL,NULL))<0) return -1;

    /* Passage de la socket de dialogue a la fonction de traitement */
    if(traitement(dialogue)<0){ shutdown(ecoute,SHUT_RDWR); return 0;}

  }
}

//parler avec le client
int gestionClientTCP(int s){

  /* Obtient une structure de fichier */
  FILE *dialogue=fdopen(s,"a+");
  if(dialogue==NULL){ perror("gestionClient.fdopen"); exit(EXIT_FAILURE); }

  /* Echo */
  char ligne[MAX_LIGNE];
  while(fgets(ligne,MAX_LIGNE,dialogue)!=NULL){
    fprintf(dialogue,"> %s",ligne);
    printf("catch = %s\n",ligne);
  }

  /* Termine la connexion */
  fclose(dialogue);
  return 0;
}

//client TCP
int connexionServeurTCP(char *hote,char *service){
  struct addrinfo precisions,*resultat,*origine;
  int statut;
  int s;

  /* Creation de l'adresse de socket */
  memset(&precisions,0,sizeof precisions);
  precisions.ai_family=AF_UNSPEC;
  precisions.ai_socktype=SOCK_STREAM;
  statut=getaddrinfo(hote,service,&precisions,&origine);
  if(statut<0){ perror("connexionServeur.getaddrinfo"); exit(EXIT_FAILURE); }
  struct addrinfo *p;
  for(p=origine,resultat=origine;p!=NULL;p=p->ai_next)
    if(p->ai_family==AF_INET){ resultat=p; break; }

  /* Creation d'une socket */
  s=socket(resultat->ai_family,resultat->ai_socktype,resultat->ai_protocol); 
  if(s<0){ perror("connexionServeur.socket"); exit(EXIT_FAILURE); }

  /* Connection de la socket a l'hote */
  if(connect(s,resultat->ai_addr,resultat->ai_addrlen)<0) return -1;

  /* Liberation de la structure d'informations */
  freeaddrinfo(origine);

  return s; 
}
