#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>

#include <netinet/tcp.h>

#define MAX_LIGNE 1
#define MAX_UDP_MESSAGE 10
#define MAX_TAMPON 100


//serveur UPD
int initialisationSocketUDP(char *service){
  struct addrinfo precisions,*resultat,*origine;
  int statut;
  int s;

  /* Construction de la structure adresse */
  memset(&precisions,0,sizeof precisions);
  precisions.ai_family=AF_UNSPEC;
  precisions.ai_socktype=SOCK_DGRAM; //Unix
  precisions.ai_flags=AI_PASSIVE;
  statut=getaddrinfo(NULL,service,&precisions,&origine);
  if(statut<0){ perror("initialisationSocketUDP.getaddrinfo"); exit(EXIT_FAILURE); }
  struct addrinfo *p;
  for(p=origine,resultat=origine;p!=NULL;p=p->ai_next)
    if(p->ai_family==AF_INET){ resultat=p; break; }

  /* Creation d'une socket */
  s=socket(resultat->ai_family,resultat->ai_socktype,resultat->ai_protocol);
  if(s<0){ perror("initialisationSocketUDP.socket"); exit(EXIT_FAILURE); }

  /* Options utiles */
  int vrai=1;
  if(setsockopt(s,SOL_SOCKET,SO_REUSEADDR,&vrai,sizeof(vrai))<0){
    perror("initialisationServeurUDPgenerique.setsockopt (REUSEADDR)");
    exit(-1);
  }

  /* Specification de l'adresse de la socket */
  statut=bind(s,resultat->ai_addr,resultat->ai_addrlen);
  if(statut<0) {perror("initialisationServeurUDP.bind"); exit(-1);}

  /* Liberation de la structure d'informations */
  freeaddrinfo(origine);

  return s;
}

int boucleServeurUDP(int s,int (*traitement)(unsigned char *,char *, int)){
  while(1){
    struct sockaddr_storage adresse;
    socklen_t taille=sizeof(adresse);
    unsigned char message[MAX_UDP_MESSAGE];
    int nboctets=recvfrom(s,message,MAX_UDP_MESSAGE,0,(struct sockaddr *)&adresse,&taille);
    char * hote;
    char * service;
    switch (adresse.ss_family){
	case AF_INET:	    
	    hote=malloc(MAX_TAMPON);
	    if(hote==NULL){ perror("socketVersClient.malloc"); exit(EXIT_FAILURE); } 
	    service=malloc(MAX_TAMPON);
	    if(service==NULL){ perror("socketVersClient.malloc"); exit(EXIT_FAILURE); }
	    getnameinfo((struct sockaddr *)&adresse,sizeof(adresse),hote,MAX_TAMPON,service,MAX_TAMPON,NI_NUMERICHOST|NI_NUMERICSERV);
	    // printf("adresse %s service %s\n",hote,service );
	    break;
        default:
	    //printf("oups\n");
	    break;
    }
    if(nboctets<0) return -1; 
    if(traitement(message,hote, nboctets)<0){
	free(hote);
	free(service);
	break;
    }
    free(hote);
    free(service);
  }
  return 0;
}

int creationSocketClientUDP(char *hote,char *service,void ** handle){
  struct addrinfo precisions,*resultat,*origine;
  int statut;
  int s;

  /* Creation de l'adresse de socket */
  memset(&precisions,0,sizeof precisions);
  precisions.ai_family=AF_UNSPEC;
  precisions.ai_socktype=SOCK_DGRAM;
  statut=getaddrinfo(hote,service,&precisions,&origine);
  if(statut<0){ perror("messageUDPgenerique.getaddrinfo"); exit(EXIT_FAILURE); }
  struct addrinfo *p;
  for(p=origine,resultat=origine;p!=NULL;p=p->ai_next)
    if(p->ai_family==AF_INET){ resultat=p; break; }

  /* Creation d'une socket */
  s=socket(resultat->ai_family,resultat->ai_socktype,resultat->ai_protocol);
  if(s<0){ perror("messageUDPgenerique.socket"); exit(EXIT_FAILURE); }

  /* Option sur la socket */
  int vrai=1;
  if(setsockopt(s,SOL_SOCKET,SO_BROADCAST,&vrai,sizeof(vrai))<0){
    perror("initialisationServeurUDPgenerique.setsockopt (BROADCAST)");
    exit(-1);
  }

  * handle = malloc(sizeof(struct addrinfo));
  *(struct addrinfo *) * handle = *resultat;
  
  /* Liberation de la structure d'informations */
  freeaddrinfo(origine);
  
  return s;
}

void fermeSocketClientUDP(int s, void * handle){
  /* Fermeture de la socket d'envoi */
  close(s);
  free(handle);
}

int envoieMessageUDP(int s, void * handle, unsigned char * message, int taille){
  struct addrinfo * resultat = handle;
  /* Envoi du message */
  int nboctets=sendto(s,message,taille,0,resultat->ai_addr,resultat->ai_addrlen);
  if(nboctets<0){ perror("messageUDPgenerique.sento"); exit(EXIT_FAILURE); }
  return nboctets;
}
