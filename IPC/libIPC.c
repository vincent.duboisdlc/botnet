#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>

#define TAILLE_MSG_MAX 1024
#define TYPE_LIRE_PROCHAIN 0
#define TYPE_DEMANDE_CONNEXION 1
#define TYPE_VALIDATION_CONNEXION 2
#define TYPE_LISTE_BOT 3
#define TYPE_ACTIVATION_CHARGE 4


typedef struct {
    long type;
    char texte[TAILLE_MSG_MAX];
} FDMmsg;

///////////////////////
//////////FDM//////////
/////////////////////// 

int FDM_get(key_t FDMkey) {
    int FDMid = msgget(FDMkey, 0666);
    if (FDMid == -1) {
	perror ("FDM existe pas!");
	exit(1);
    }
    return FDMid;
}

int FDM_create(key_t FDMkey) {
    int FDMid = msgget(FDMkey, IPC_CREAT|IPC_EXCL|0666);
    if (FDMid == -1) {
	perror ("FDM existe deja!");
	exit(1);
    }
    return FDMid;
}

int FDM_create_private(void) {
    return msgget(IPC_PRIVATE,IPC_CREAT|0666);
}

//Ecrire un message dans FDM
int FDM_write(int FDMid ,long type, char * texte) {
    FDMmsg commande;
    commande.type = type;
    strcpy(commande.texte,texte);
    if (msgsnd(FDMid, &commande, TAILLE_MSG_MAX, 0) == -1 ) {
	perror ("Err ecriture message!");
	exit(1);
    }
    return 0;
}

//Lire un message dans FDM
FDMmsg FDM_read(int FDMid, long type) {
    FDMmsg rcv;
    if (msgrcv(FDMid, &rcv, TAILLE_MSG_MAX, type, 0) > TAILLE_MSG_MAX) {
	perror ("Err lecture message!");
	exit(1);
    }
    return rcv;
}

//Fermer FDM
int FDM_close(int FDMid){
    return msgctl(FDMid, IPC_RMID, 0);
}

///////////////////////
//////////SHM//////////
///////////////////////

int SHM_create(key_t SHMkey, int SHMsize) {
    /*if ((SHMsize > SHM_SIZE_MAX)||(SHMsize < SHM_SIZE_MIN)) {
	perror ("SHMsize incorrecte");
	exit(1);
	}*/
    int SHMid = shmget(SHMkey, SHMsize, IPC_CREAT|IPC_EXCL|0666);
    if (SHMid == -1) {
	/*perror ("SHM existe deja!");
	  exit(1);*/
	return -1;
    }
    return SHMid;
}

int SHM_get(key_t SHMkey, int SHMsize) {
    /*if ((SHMsize > SHM_SIZE_MAX)||(SHMsize < SHM_SIZE_MIN)) {
	perror ("SHMsize incorrecte");
	exit(1);
	}*/
    int SHMid = shmget(SHMkey, SHMsize, 0666);
    if (SHMid == -1) {
	/*perror ("SHM existe pas!");
	  exit(1);*/
	return -1;
    }
    return SHMid;
}

char * SHM_write_char(int SHMid, char* data, int size) {
    int SHMsize = size;
    char * SHMmsg = shmat(SHMid, 0, 0);
    if (SHMmsg == (char *)-1) {
	perror("SHM attache"); 
	exit(1);
    }
    for(int i=0;i<SHMsize;i++){
	SHMmsg[i] = data[i];
    }
    return SHMmsg;
}

unsigned char * SHM_write_uchar(int SHMid, unsigned char* data, int size) {
    int SHMsize = size;
    unsigned char * SHMmsg = shmat(SHMid, 0, 0);
    if (SHMmsg == (char *)-1) {
	perror("SHM attache"); 
	exit(1);
    }
    for(int i=0;i<SHMsize;i++){
	SHMmsg[i] = data[i];
    }
    return SHMmsg;
}

int * SHM_write_int(int SHMid, int data) {
    int * SHMmsg = (int *) shmat(SHMid, 0, 0);
    if (SHMmsg == (int *)-1) {
	perror("SHM attache");  
	exit(1);
    }
    * SHMmsg = data;
    return SHMmsg;
}

pid_t * SHM_write_pid(int SHMid, pid_t  data) {
    int * SHMmsg = (pid_t *) shmat(SHMid, 0, 0);
    if (SHMmsg == (pid_t *)-1) {
	perror("SHM attache"); 
	exit(1);
    }
    * SHMmsg = data;
    return SHMmsg;
}

void SHM_read_char(int SHMid, char ** SHMmsg) {
    *SHMmsg = (char *) shmat(SHMid, 0, SHM_RDONLY);
    if (*SHMmsg == (char *)-1) {
	perror("SHM attache");
	exit(1);
    }
}

void SHM_read_uchar(int SHMid, unsigned char ** SHMmsg) {
    *SHMmsg = (unsigned char *) shmat(SHMid, 0, SHM_RDONLY);
    if (*SHMmsg == (unsigned char *)-1) {
	perror("SHM attache");
	exit(1);
    }
}

int * SHM_read_int(int SHMid) {
    int * SHMmsg = (int *) shmat(SHMid, 0, SHM_RDONLY);
    if (*SHMmsg == (int *)-1) {
	perror("SHM attache");
	exit(1);
    }
    return SHMmsg; 
}

pid_t * SHM_read_pid(int SHMid) {
    pid_t * SHMmsg = (pid_t *) shmat(SHMid, 0, SHM_RDONLY);
    if (*SHMmsg == (pid_t *)-1) {
	perror("SHM attache");
	exit(1); 
    }
    return  SHMmsg;
}

void SHM_remove_char(int SHMid, char ** ptSHMmsg) {
    shmdt((char*) *ptSHMmsg);
    shmctl(SHMid,IPC_RMID,NULL); 
}

void SHM_remove_uchar(int SHMid, unsigned char ** ptSHMmsg) {
    shmdt((unsigned char*) *ptSHMmsg);
    shmctl(SHMid,IPC_RMID,NULL); 
}

void SHM_remove_int(int SHMid, int ** ptSHMmsg) {
    shmdt((int*) *ptSHMmsg);
    shmctl(SHMid,IPC_RMID,NULL);  
}

void SHM_remove_pid(int SHMid, pid_t ** ptSHMmsg) {
    shmdt((pid_t*) *ptSHMmsg);
    shmctl(SHMid,IPC_RMID,NULL); 
}
 
void SHM_detach_char(char ** ptSHMmsg) {
    shmdt(*ptSHMmsg);
}

void SHM_detach_uchar(unsigned char ** ptSHMmsg) {
    shmdt(*ptSHMmsg);
}

void SHM_detach_int(int ** ptSHMmsg) {
    shmdt(*ptSHMmsg);
}

void SHM_detach_pid(pid_t ** ptSHMmsg) {
    shmdt(*ptSHMmsg);
}
