#define TAILLE_MSG_MAX 1024
#define TYPE_LIRE_PROCHAIN 0
#define TYPE_DEMANDE_CONNEXION 1
#define TYPE_VALIDATION_CONNEXION 2
#define TYPE_LISTE_BOT 3
#define TYPE_ACTIVATION_CHARGE 4
#define TYPE_UPLOAD_CHARGE 5
#define TYPE_DELET_CHARGE 6
#define TYPE_ETAT_BOT 7
#define TYPE_RESULTAT_BOT 8
#define TYPE_DOWNLOAD_CHARGE 9
#define TYPE_QUIT_BOT 10
#define TYPE_ACTIVATION_LISTE_CHARGE 11

key_t FDM1key = 2000;

key_t SHMkeyFile = 4000;
key_t SHMkeySizeFile = 4001;
key_t SHMkeyName = 4002;
key_t SHMkeySizeName = 4003;
key_t SHMkeyPIDCandC = 4004;
key_t SHMkeyCmd = 4005;
key_t SHMkeySizeParam1 = 4006;
key_t SHMkeySizeParam2 = 4007;
key_t SHMkeyParam1 = 4008;
key_t SHMkeyParam2 = 4009;
key_t SHMkeySizeResult = 4010;
key_t SHMkeyResult = 4011;

typedef struct {
    long type;
    char texte[TAILLE_MSG_MAX];
} FDMmsg;


int FDM_get(key_t FDMkey);

int FDM_create(key_t FDMkey);

int FDM_create_private(void);

int FDM_write(int FDMid ,long type, char * texte);

FDMmsg FDM_read(int FDMid, long type);

int FDM_close(int FDMid);

int SHM_create(key_t SHMkey, int SHMsize);

int SHM_get(key_t SHMkey, int SHMsize);

char * SHM_write_char(int SHMid, char* data, int size);

unsigned char * SHM_write_uchar(int SHMid, unsigned char* data, int size);

int * SHM_write_int(int SHMid, int data);

pid_t * SHM_write_pid(int SHMid, pid_t data);

void SHM_read_char(int SHMid, char ** ptSHMmsg);

void SHM_read_uchar(int SHMid, unsigned char ** ptSHMmsg);

int * SHM_read_int(int SHMid);

pid_t * SHM_read_pid(int SHMid);

void SHM_remove_char(int SHMid, char ** ptSHMmsg);

void SHM_remove_uchar(int SHMid, unsigned char ** ptSHMmsg);

void SHM_remove_int(int SHMid, int ** ptSHMmsg);

void SHM_remove_pid(int SHMid, pid_t ** ptSHMmsg);

void SHM_detach_char(char ** ptSHMmsg);

void SHM_detach_uchar(unsigned char ** ptSHMmsg);

void SHM_detach_int(int ** ptSHMmsg);

void SHM_detach_pid(pid_t ** ptSHMmsg);
 
