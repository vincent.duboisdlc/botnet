//#define NB_SEM_MAX 50

typedef struct flux {
    void(*action)(void*);
    void* params;
} Flux;

void SEM_init(pthread_mutex_t* ptSem);

void SEM_lock(pthread_mutex_t* ptSem);

void SEM_unlock(pthread_mutex_t* ptSem);

void lancerFlux(void (*fonction)(void*), void *parametres, int size);
