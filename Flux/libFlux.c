#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>

//#define NB_SEM_MAX 50

typedef struct flux {
    void(*action)(void*);
    void* params;
} Flux;

///////////Semaphore///////////////

void SEM_init(pthread_mutex_t* ptSem){
  pthread_mutex_init(ptSem, NULL);
}

void SEM_lock(pthread_mutex_t* ptSem){
  int ret = pthread_mutex_lock(ptSem);
  if(ret == EINVAL) {
      SEM_init(ptSem);
      SEM_lock(ptSem);
  }
}

void SEM_unlock(pthread_mutex_t* ptSem){
  int ret = pthread_mutex_unlock(ptSem);
  if(ret != 0){
    perror ("unlock failed");
    exit(1);
  }
}


///////////Thread///////////////

static void* executeAction(void* args) {
  Flux *F = args;
  //bloquer semaphores
  F->action(F->params);
  //debloquer semaphores
  if(F->params != NULL) free(F->params);
  free(F);
  pthread_exit(NULL);
}

void lancerFlux(void (*fonction)(void*), void *parametres, int size) {
  // Déclaration du thread et ses attributs
  pthread_t tid;
  pthread_attr_t attr;

  //definition des arguments
  Flux *F = malloc(sizeof(struct flux));
  F->action = fonction;
  if(size > 0){
      F->params = malloc(size);
      memcpy(F->params, parametres, size);
    }
  else F->params = NULL;

  //Detachement et lancement thread
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
  pthread_create(&tid, &attr, executeAction, F);
  //pthread_join(tid, NULL);
}
