Projet Systeme Reseau - Damien TILLAUX - Vincent DUBOIS
=======================================================

## Compiler
- Lire le README

<pre>
make man
</pre>

- Bibliotheques : TCP, UDP, IPC et FLUX

<pre>
make lib
</pre>

- Bot, Admin, C&C et ServerWeb

<pre>
make
</pre>

- Clean les exécutables et librairies statiques

<pre>
make clean
</pre>

## Execution
Lancer dans 4 terminaux différents:

<pre>
- Lancer en premier le C&C:

cd CandC/

./CandC

- Si utilisation du Serveur Web:

cd ServeurWeb/

./http

Pour accéder a la page web: 127.0.0.1:4050/serveurJCvindam

- Pour lancer le bot sur une machine:

cd Bot/

./bot

ou si vous voulez lancer un deuximèe bot avec un autre id

./bot2

ATTENTION:

Pour que l'exécution des charges du bot se fasse il faut exporter cette commande avant de lancer le bot:

export LD_LIBRARY_PATH={chemin absolut vers la localisation de notre projet}/Bot/charge:$LD_LIBRARY_PATH

exemple:

export LD_LIBRARY_PATH=/home/pifou/projet/Bot/charge:$LD_LIBRARY_PATH

- Pour lancer une commande en admin:

cd Admin/

./admin {commande}

</pre>

## Type de commande
Les commandes sont les même sur le Serveur http et l'admin shell à part pour enregister une charge sur le C&C.

- Voir la liste des Bots en train d'émettre:

-l

Return:

La liste des bots.

- Voir l'état d'un bot:

-e {id ou ip du bot}

Return:

Le nombre de charge installée, le nombre de charge exécutée, le temps de vie du bot.

- Exécuter une charge sur un bot:

-a {id ou ip du bot} {nom de la charge}

Return:

Id du bot et id d'exécution de la charge.

- Exécuter une charge sur un ou plusieur bots:

-al {nom de la charge} {id ou ip du bot} {id ou ip du bot} ...

Return:

La liste d'id des bots avec l'id d'exécution de la charge sur ces bots.

- Voir le resultat de l'exécution d'une charge sur un bot:

-r {id ou ip du bot} {id de l'exécution}

Return:

Les ecritures de l'exécution de la charge dans stdout.

- Enregister une charge dans le C&C avant de l'upload dans un bot:

Pour l'admin:

-dl {chemin absolut vers le fichier}

Pour le Serveur http:

-cliquer sur "Choisir un fichier"

-choisir votre fichier.

-appyer sur "Valider"


Return:

Message d'indication sur l'état d'enregistrement de la charge

- Upload une charge sur un bot:

-u {id ou ip du bot}

Return:

Message d'indication sur l'état d'upload de la charge

- Retirer une charge d'un bot:

-d {id ou ip du bot} {nom de la charge}

Return:

Message d'indication sur l'état de suppression de la charge.

- Kill la connexion avec un bot:

-q {id ou ip du bot}

## Problèmes/améliorations
Plusieurs petites améloirations peuvent être ammenées et elles sont rapide à implémenter:

	  - Remonter l'erreur sur un NOKid lors de la demande de résultat d'exécution sur un bot qui ne possède pas cette id en mémoire. Cela nécéssite alors de faire un make ipcrm dans CandC/ et de relancer le C&C et le Serveur http. Une remonté de l'erreur vers le Serveur http ou l'admin peut facilement être mis en place.
	  
	  - Lors de la communication entre le serveur http et le C&C la communication du serveur vers la C&C ce fait par interruption et dans l'autre sens juste en regardant si un segment mémoire a été créé. Regarder si un segment mémoire a été crée peut causer quelque prblème de temps en temps. Il faudrait passer par une interruption mais cela aurait nécessité de revoir la structure du code du serveur.
	  
	  - Lors de l'exécution d'une charge sur un bot, celui ci doit bloquer le stdout pour ne pas recevoir des sorties parasites des autres exécution car nous avons threader notre exécution du bot. Cela peut être problématique dans la cas d'une charge avec une bloque infinie car l'exécution de cette charge sera infini et les autres charges ne seront jamais exécutées. Pour résoudre cela il faudrait forker les exécutions des charges pour que chaque exécution aient son propre stdout.
	  
	  - Lors de la compilation 2 types de warning apparaissent. L'un sur une variable non utilisé car la structure demande un argument mais ici nous n'en avons pas besoin. Le deuximèe warning vient lors de la compilation de la lib IPC mais cela ne cause pas de problème mais peut surement être enlevé.

## Travail
Pendant le confinement, nous avons principalement travaillé a deux sur une seule machine via partage d'écran. C'est pour ca que vous constaterez qu'il n'y a qu'une seule personne qui effectue les commits.
De plus, le PC que nous utilisions en salle de TP avait pour utilisateur par defaut **sdardenn** ce qui explique les commits du debut de projet.

Minimum 15h de travail par semaine sur la projet étaient faites lors du confinement. Donc à notre avis, finir le projet n'aurait pas été possible sans le confinenement. Nous espérons que cette remarque sera prise en compte pour l'année prochaine pour un projet aussi complet mais avec moins de superflux pour le réaliser avec moins de temps.

## Test
Nous avons tester nos codes avec le groupe de Richard Simonin et Guillaume Rouille sur les Zabeth. La communication entre nos C&C et Bot fonctionnait parfaitement. 